### Jeremy Delobel && Maximilien Weinmann

[Lien vers le gitlab](https://gitlab.univ-lille.fr/maximilien.weinmann.etu/act_delobel_weinmann/-/tree/main/TP4)

Pour une lecture plus agréable, nous vous conseillons de lire ce Readme dans Visual Studio Code, en mode prévisualisation.

### Lancement de code

Verifier de bien se mettre dans le dossier TP4.
```r
#Executer les commandes suivantes :
javac *.java
# Puis lancer en spécifiant un jeu de donnée au choix
java Main ./SMTWP/n100_15_b.txt
# ou encore 
java Main ./SMTWP/n1000_2_b.txt
```

## __*1 Description du problème*__

##### __On rappelle que :__

Sauf précisé autrement, __une indication de ligne dans ce Readme fait référence au fichier Main.java__

- n est le nombre de task
- p<sub>j</sub> est le temps d'éxécution de la tâche j
- w<sub>j</sub> est le poids de la tâche j
- d<sub>j</sub> est le temps limite d'éxécution de la tâche j
- C<sub>j</sub> est le temps de complétion de la tâche j
- T<sub>j</sub> est le retard de la tâche, il vaut minimum 0


### __Notre Certificat__
Le certificat pour notre problème d'ordonnancement sera une liste de taille n fois la taille de la donnée, qui est en log(n), donc le certificat sera de taille n*log(n).  
Cette liste détient les indices des tâches à éxécuter de gauche à droite.

Exemple :  

L'Ordonnancement est [2, 1, 3]
Donc la tâche 2 sera éxécutée, puis la tâche 1, et enfin la tâche 3.  
<br></br>

## __*Travail demandé*__

## <u>*Travail préliminaire*</u>
<br></br>

### __Q1.1) Écrire un programme qui prend en argument un ordonnacement O et qui calcule la fonction f(O) pour une instance donnée du problème.__

C'est fait, il s'agit de la méthode SoluceQuality() ligne 547 dans Main.java, à savoir :

``````java
public int soluceQuality(int[] Ordonnancement, Task[] ourTasks) {
    int total = 0;
    int timeSinceStart = 0;
    for (int task_index : Ordonnancement) {
        total += ourTasks[task_index].weight * Math.max((ourTasks[task_index].execution_time + timeSinceStart) - ourTasks[task_index].time_limit, 0);
        timeSinceStart += ourTasks[task_index].execution_time;
    }
    return total;
}
``````

### __Q1.2) Ecrire un programme qui permet de générer une solution aléatoire et d’évaluer sa qualité.__

C'est fait, il s'agit de la méthode generateRandomSoluce() dans Main.java, ligne 560, à savoir :

``````java
public int [] generateRandomSoluce(Task [] data) {
    int [] soluce = new int [data.length];
    for (int i = 0; i < data.length; i++) {
        soluce[i] = i;
    }
    for (int i = 0; i < data.length; i++) {
        int randomIndexToSwap = rand.nextInt(soluce.length);
        int temp = soluce[randomIndexToSwap];
        soluce[randomIndexToSwap] = soluce[i];
        soluce[i] = temp;
    }
    return soluce;
}
``````

ainsi que cette partie du Main() pour évaluer sa qualité, ligne 646 :
``````java
public static void main(String[] args) throws IOException {
    // il y a du code avant
    System.out.println("Durée de génération en seconde et résultat évaluation solution aléatoire : ");
    int[] une_solution = generateRandomSoluce(tasks);
    // il y a du code entre deux désormais, pour la meseure du temps et l'affichage
    System.out.println(soluceQuality(une_solution, tasks));
    // il y a du code après
}
``````

<br></br>

Résultats de l'aléatoire, cela servira de base de comparaison :  

| Heuristique / fichier	| n100_15_b.txt 	| n100_19_b.txt 	| n100_39_b.txt 	| n100_89_b.txt 	| n1000_1_b.txt 	| n1000_2_b.txt 	|
|---------------------	|---------------	|---------------	|---------------	|---------------	|---------------	|---------------	|
| Aléatoire             | 462236            | 932996            | 511696            | 497099            | 3043932008        | 3138162873        |

<br></br>
Durée en seconde de l'aléatoire, cela servira de base de comparaison temporelle :  

| Heuristique / fichier | n100_15_b.txt 	| n100_19_b.txt 	| n100_39_b.txt 	| n100_89_b.txt 	| n1000_1_b.txt 	| n1000_2_b.txt 	|
|---------------------	|---------------	|---------------	|---------------	|---------------	|---------------	|---------------	|
| Aléatoire             | 1.39 E-4          | 1.28 E-4          | 1.41 E-4          | 1.22 E-4          | 8.63 E-4          | 8.92 E-4          |

<br></br>

## <u>*Heuristiques constructives et recherche locale simple*</u>
### __Q1.3) Proposer et implémenter une (ou plusieurs) heuristiques constructives.__
### __indication: Pensez à prendre en compte la valeur du retard d’une tâche dans l’ordonnacement.__
<br></br>
Nous proposons 4 heuristiques constructives, toutes prennent en compte le retard d'une tâche
dans l'ordonnancement.  

- <u>Heuristique 1</u> : On range les taches par retard, on prend la tache la plus en retard, si plusieurs taches ont le même retard, on prend la tache la plus lourde, en cas d'égalité de poids, on prend la derniere de la liste rangée par ordre croissant de poids. __(cf. heuristiqueConstructive1() ligne 22)__
<br></br>

- <u>Heuristique 2</u> : On range les taches par retard, on prend la tache la plus en retard, si plusieurs taches ont le même retard, on prend la tache la plus lourde, en cas d'égalité de poids, on prend celle avec la plus grande date limite __(cf. heuristiqueConstructive2() ligne 69)__
<br></br>

- <u>Heuristique 3</u> : On range les taches par retard, on prend la tache la plus en retard, si plusieurs taches ont le même retard, on prend la tache avec le plus petit ratio poids/date limite, en cas d'égalité de ratio, on prend la première tâche de la liste rangée par ordre croissant de ratio. __(cf. heuristiqueConstructive3() ligne 122)__  
<br></br>

- <u>Heuristique 4</u> : On range les tâches par limite de temps, dans l'ordre croissant, on garde en mémoire les 20 taches ayant la plus petite limite de temps (20 est arbitraire, on aurait pu dire 40), parmi ces 20, on prend la tâche la plus lourde, en cas d'égalité de poids, en cas d'égalité de poids, on prend la tâche la moins en retard, en cas d'égalité de retard, on prend la première de la liste sachant qu'elle est rangée par retard en ordre croissant. __(cf. heuristiqueConstructive4() ligne 162)__
<br></br>

Précision : par égalité de poids, (et cela s'applique aux autres propriétes), on veut dire que parmi les plus lourdes, plusieurs se partagent la première place, les égalités ne concernant pas la première place ne nous intéressent pas.    

<br></br>
Résultats des Heuristiques constructives :  


| Heuristique / fichier | n100_15_b.txt 	| n100_19_b.txt 	| n100_39_b.txt 	| n100_89_b.txt 	| n1000_1_b.txt 	| n1000_2_b.txt 	|
|---------------------	|---------------	|---------------	|---------------	|---------------	|---------------	|---------------	|
| Aléatoire             | 483609            | 924136            | 490256            | 466351            | 3304938296        | 3207919143        |
| Heuristique 1       	| 458357           	| 1017546          	| 502902           	| 228407          	| 1761264803     	| 1981716746       	|
| Heuristique 2       	| 416924           	| 1004049          	| 489119           	| 231871           	| 1762958772       	| 1979018608       	|
| Heuristique 3       	| 296708           	| 775144           	| 322001           	| 195532           	| 3067107031      	| 3201261437       	|
| Heuristique 4       	| 288895           	| 783591           	| 250451           	| 126366           	| 1131909814      	| 1190825341       	|

<br></br>
Constatations :  

Par efficacité, on parle de qualité d'ordonnancement.  

L'heuristique 1 est entre aussi efficace que l'aléatoire et 2 fois plus efficace.  
L'heuristique 2 est semblable à l'heuristique 1.  
L'heuristique 3 est meilleure que les heuristiques 1 et 2 tant que n est petit, on voit que quand on passe à n = 1000, l'heuristique 3 vaut de l'aléatoire.  
L'heuristique 4 est de manière générale meilleure (voir bien meilleure) que toutes les autres quel que soit n, ou alors proche du meilleur.  
<br></br>
L'heuristique 4 semble être notre meilleure heuristique.  

On voit une bonne amélioration déja entre l'Aléatoire et l'heuristique 4, qui est parfois 15% plus efficace, et d'autre fois fois 2 ou 3 fois plus efficace.  

<br></br>
Durée en seconde des Heuristiques constructives :  

| Heuristique / fichier | n100_15_b.txt 	| n100_19_b.txt 	| n100_39_b.txt 	| n100_89_b.txt 	| n1000_1_b.txt 	| n1000_2_b.txt 	|
|---------------------	|---------------	|---------------	|---------------	|---------------	|---------------	|---------------	|
| Aléatoire             | 1.15 E-4          | 1.16 E-4          | 1.07 E-4          | 1.19 E-4          | 9.35 E-4          | 8.92 E-4          |
| Heuristique 1       	| 0.023           	| 0.021          	| 0.021           	| 0.020          	| 0.223         	| 0.192           	|
| Heuristique 2       	| 0.010           	| 0.006          	| 0.008           	| 0.007           	| 0.201         	| 0.195         	|
| Heuristique 3       	| 0.005           	| 0.005           	| 0.006           	| 0.006           	| 0.058         	| 0.061         	|
| Heuristique 4       	| 0.008           	| 0.009           	| 0.007           	| 0.008           	| 0.206         	| 0.191         	|

<br></br>
### Constatations :
<br></br>
- Le temps augmente de manière raisonnable entre 100 et 1000 taches, ce n'est pas linéaire, mais cela reste une augmentation polynomiale

- L'algorithme aléatoire est de loin le plus rapide dans tous les cas.  

- L'heuristique 1 en plus d'être la moins efficace des 4 heuristiques constructives, est la plus lente.  

- L'heuristique 2 est plus rapide que l'heuristique 1 sauf quand n est grand, où elle est aussi lente que l'heuristique 1, car on rentre plus fréquement dans son pire des cas.  

- L'heuristique 3 est toujours la plus rapide mais est aussi efficace que l'aléatoire quand n est grand.  

- L'heuristique 4 est aussi rapide que l'heuristique 2, mais son efficacité est toujours meilleure ou proche de l'égalité avec l'heuristique 3.  
<br></br>

En terme de Complexité temporelle, les heuristiques 2, 3 et 4 sont les plus intéressants, la meilleure étant l'heuristique 3.  


<strong>En prenant en compte la complexité temporelle ainsi que la qualité de l'ordonnancement, on voit que :</strong>  

- L'heuristique 2 est simplement meilleure que l'heuristique 1, l'ordonnancement est de même qualité mais l'heuristique 2 est au pire aussi rapide, elle peut être plus de 3 fois plus rapide que l'heuristique 1.  

- L'heuristique 3 est toujours la plus rapide mais offre un ordonnancement de mauvaise qualité (comme l'aléatoire) quand n est grand.  

- L'heuristique 4 offre souvent un ordonnancement de meilleurs qualité que les autres, en étant rapide comme l'heuristique 2.  

- L'heuristique 4 offre le meilleur ordonnancement quand n et grand, environ 60% plus efficace que l'heuristique 1 et 2 quand n = 1000, sachant qu'ils sont eux meme 60% plus efficace que l'heuristique 3 et l'aléatoire quand n = 1000.  

<br></br>
L'heuristique 4 est donc la plus intéressante de toutes.  
L'heuristique 4 sera donc aussi une base de comparaison pour la suite.  
<br></br>

### __Q1.4) Proposer et implémenter (au moins) un voisinage pour la conception d’heuristiques par recherche locale simple. En déduire une (ou plusieurs) recherche locale simple de type HillClimbing ou VND. Veillez à spécifier les différents choix de conception (initialisation, type de mouvement, etc).__
<br></br>
Nous proposons 2 algorithmes de recherche local simple, Hillclimbing et VND, avec pour chacun plusieurs possibilités différentes de mouvement :  

- <strong>HillClimbing</strong> : Nous choisissons comme <span style="color:red">solution initiale celle donnée par l'heuristique constructive 4</span>, et comme le HillClimbing le veut, nous parcourons ses voisins pour voir s'il en a un meilleur que lui, <span style="color:red">le voisinage de permutation est généré par inversion</span>, ce voisinage est plus rapide a générer, on parle de O(n), comparé à O(n<sup>2</sup>) pour un voisinage par swap ou insertion, <span style="color:red">la règle de pivot est un Best Improvement</span> __(cf. HillClimbing() ligne 246)__
  

- <strong>HillClimbing First improvement</strong> : La seule différence avec le Hillclimbing est que <span style="color:red">la règle de pivot est un First Improvement</span> __(cf. HillClimbing() ligne 246)__

- <strong>HillClimbing Worse improvement</strong> : La seule différence avec le Hillclimbing est que <span style="color:red">la règle de pivot est un Worse Improvement</span> __(cf. HillClimbing() ligne 246)__


- <strong>Variable Neighborhood Descent</strong> : Nous choisissons comme <span style="color:red">solution initiale celle donnée par l'heuristique constructive 4</span> aussi, <span style="color:red">le voisinage de permutation est généré par inversion</span>, mais si on ne trouve pas de meilleur voisin, l'inversion se fait entre tache de moins en moins voisine (pour chaque tache, au lieu de regarder le voisin à i + 1, on regarde le voisin à i + 2, si dans ce voisinage là on a toujours pas de meilleur voisin, on regarde à i + 3 etc jusqu'à i + n - 1), <span style="color:red">la règle de pivot est un Best Improvement</span> __(cf. VND() ligne 401)__

- <strong>Variable Neighborhood Descent First Improvement</strong> : La seule différence avec le premier VND est que <span style="color:red">la règle de pivot est un First Improvement</span> __(cf. VND() ligne 401)__

(On applique déja ce que dit le principe dans le cours pour les ILS : un bon optimum local ne doit pas être très loin d'un autre probablement encore meilleur, par conséquent nous prenons l'heuristique constructive 4 pour solution initiale, de plus quelques test avec les autres heuristiques constructives ne nous ont pas convaincus)  
<br></br>

### Résultats des Heuristiques par recherche locale simple :  
Vous pouvez voir qu'une heuristique de recherche locale est par défaut en Best Improvement, sauf en cas de précision dans le nom
<br></br>


| Heuristique / fichier | n100_15_b.txt 	| n100_19_b.txt 	| n100_39_b.txt 	| n100_89_b.txt 	| n1000_1_b.txt 	| n1000_2_b.txt 	|
|---------------------	|---------------	|---------------	|---------------	|---------------	|---------------	|---------------	|
| Aléatoire             | 483609            | 924136            | 490256            | 466351            | 3304938296        | 3207919143        |
| Heuristique 4       	| 288895           	| 783591           	| 250451           	| 126366           	| 1131909814      	| 1190825341       	|
| HillClimbing          | 197974           	| 498938           	| 181557           	| 81130           	| 770615224      	| 851554125       	|
| HillClimbing First Imp| 189902           	| 492636           	| 174987           	| 77914           	| 752848960      	| 783108505       	|
| HillClimbing Worse Imp| 193483           	| 546751           	| 184056           	| 80675         	| 804912022        	| 869345734        	|
| VND                  	| 173564           	| 479886           	| 153898           	| 56522           	| 667520216      	| 688303787       	|
| VND First Improvement	| 173564           	| 481024           	| 153898           	| 56522           	| 666244661      	| 688192107       	|


<br></br>

Constatations :  
On rappelle que l'efficacité fait référence à la qualité de l'ordonnancement
<br></br>

- HillClimbing trouve toujours un meilleur ordonnancement que l'heuristique 4, étant environ 30% plus efficace

- HillClimbing First Improvement trouve toujours un score un peu meilleur (au mieux de 10%) que HillClimbing.

- HillClimbing Worse Improvement trouve un score semblable au Hillclimbing (Best improvement) quand n = 100, mais le plus mauvais des scores par recherche locale simple quand n = 1000

- VND est toujours meilleur qu'HillClimbing, entre 3 et 30% plus efficace que HillClimbing, il est aussi toujours meilleur que Hillclimbing First improvement.

- VND First Improvement est très similaire à VND, il a plusieurs fois eu le même résultat, et quand le résultat n'est pas le même, il est très très proche.  
<br></br>

En terme d'ordonnancement, les VND sont nos meilleurs algorithmes jusque ici.
Nous avons vu une bonne amélioration déja entre l'Aléatoire et l'heuristique 4, qui est parfois 15% plus efficace, et d'autre fois fois 2 ou 3 fois plus efficace que l'aléatoire (voir Q1.3), mais les VND peuvent être plus de 2 fois plus efficaces que l'heuristique 4, au pire ils offrent un score d'ordonnancement 40 % meilleur.  

Nous avons l'algorithme aléatoire, qui donne un ordonnancement rapide, l'algorithme Heuristique constructive 4 qui est le meilleur de nos heuristiques constructives
et les VND qui sont nos meilleurs de nos algorithmes par recherche locale simple en terme d'efficacité et bien sûr offre un meilleur ordonnancement que la simple heuristique constructive 4.  
<br></br>

Durée en seconde des Heuristiques par recherche locale simple :  
<br></br>


| Heuristique / fichier | n100_15_b.txt 	| n100_19_b.txt 	| n100_39_b.txt 	| n100_89_b.txt 	| n1000_1_b.txt 	| n1000_2_b.txt 	|
|---------------------	|---------------	|---------------	|---------------	|---------------	|---------------	|---------------	|
| Aléatoire             | 1.29 E-4          | 1.07 E-4          | 1.07 E-4          | 1.19 E-4          | 9.35 E-4          | 1.01 E-3          |
| Heuristique 4       	| 0.009           	| 0.007           	| 0.007          	| 0.008           	| 0.222         	| 0.148         	|
| HillClimbing       	| 0.042           	| 0.070           	| 0.038           	| 0.036           	| 52.251        	| 47.781        	|
| HillClimbing First Imp| 0.017           	| 0.030           	| 0.015           	| 0.012         	| 35.211        	| 34.624        	|
| HillClimbing Worse Imp| 0.046           	| 0.077           	| 0.046           	| 0.044         	| 52.976        	| 48.610        	|
| VND                  	| 0.104           	| 0.135           	| 0.073           	| 0.057           	| 157.831       	| 168.807       	|
| VND First Improvement	| 0.048           	| 0.038           	| 0.037           	| 0.023           	| 101.850       	| 106.907       	|

<br></br>
Constatations :

- Le Hillclimbing est entre aussi rapide ou deux fois moins rapide que le VND First Improvement quand n est petit, mais quand n est grand, il est plus rapide que le VND First Improvement, plus de 2 fois plus rapide quand n = 1000

- Le Hillclimbing First Improvement est toujours plus rapide, au pire, il fini en 70% du temps de Hillclimbing, au mieux il va 3 fois plus vite, l'écart se réduit néanmoins lorsque n grandit

- Le Hillclimbing Worse Improvement est toujours très légèrement plus lent que le Hillclimbing (Best improvement)

- Le VND est toujours le plus lent, il est dangereusement plus lent que tous les autres quand n est grand

- Le VND First Improvement est de manière générale plus rapide que les autres excepté le Hillclimbing First improvement quand n est petit, mais il est plus lent que HillClimbing quand n est grand, il reste néanmoins plus toujours rapide que VND, car il lui faut au plus seulement 60% du temps de VND, au mieux il est plus de 3 fois plus rapide que VND.  

<br></br>
En terme de complexité temporelle, les HillClimbings et VND first improvement sont les plus intéressants, surtout les HillClimbings quand n est grand et en particulier HillClimbing First Improvement.

Cependant en prenant en compte la complexité temporelle ainsi que la qualité d'ordonnancement, Hillclimbing first improvement et le VND first improvement sont les plus intéressants, ils sont simplement meilleurs que leurs versions Best Improvement.
Nous les prendrons comme base de comparaison pour la suite.

<br></br>
Il semblerait que le type de pivot Worse Improvement n'est pas intéressant pour ce probleme d'ordoonnancement car il ne se démarque pas par une meilleure vitesse que les autres, et ses score sont ou semblables aux autres quand n est petit, ou plus mauvais quand n est grand (n = 1000). Nous ne prendrons plus en considération ce type de mouvement dans la suite du TP.  

Il semblerait aussi que le type de pivot First Improvement soit plus intéressant que le Best Improvement pour ce probleme d'ordonnancement pour ce qui est de la recherche locale simple. Nous ajoutons qu'il est logique que les First improvements soient plus rapide, puis qu'une fois un améliorant trouvé, leur algorithme passe à la prochaine étape, alors que dans le cas des Best improvement, il faut parcourir tous les voisins pour etre sur de trouver le meilleur améliorant, ce qui est plus long dans quasiment tous les cas (sauf dans le cas où le seul améliorant est le dernier voisin).  
<br></br>
Les VND font une plus grande recherche de voisinage, cela explique leur plus grande efficacité, mais aussi leur lenteur.
Il reste important de mentionner que nos derniers algorithmes sont déja de l'ordre de la minute quand n = 1000.  
<br></br>

## <u>*Recherche Locale Itérée*</u>
### __Q1.5) Proposer et implémenter une recherche locale itérée, de type ILS. Veillez à spécifier les différents choix de conception (initialisation, recherche locale de base, perturbation, critère d’acceptation, critère d’arrêt).__  

<br></br>


Pour l'initialisation des ILS on utilisera toujours l'algorithme constructif 4, pour la perturbation, on fait nombre de tâches * 0.1 inversions parmi au sein de la liste de l'ordonnancement, et la condition d'arret est sur le temps d'éxécution : on arrete après (0.6 * nombre de taches) secondes (60 secondes pour 100 taches, 10 minutes pour 1000).
<span style="color:red">__Les implementations de ces différents ILS peuvent être retrouvées dans le fichier *Main.java* aux lignes 489 et 517, sous les noms ILS_Hillclimbing et ILS_VND__</span>, il s'agit de deux fonctions, dont les paramètres permettent de spécifier les variantes :  

<br></br>


- <u>ILS Hillclimbing First Improvement accept always</u> : sera abbrévié en __ILS Hil F accept alwa__. Pour la recherche locale de base, c'est Hillclimbing First improvement,  on accepte toujours le nouvel ordonnancement trouvé (diversification). 

- <u>ILS Hillclimbing First Improvement accept only Improving</u> : sera abbrévié en __ILS Hil F accept Betr__. Pour la recherche locale de base, c'est Hillclimbing First improvement,  on accepte seulement un nouvel ordonnancement trouvé s'il donne un meilleur score que l'original ou celui trouvé avant lui (intensification).

- <u>ILS Hillclimbing Best Improvement accept always</u> : sera abbrévié en __ILS Hil B accept alwa__. Pour la recherche locale de base, c'est Hillclimbing <strong>Best improvement</strong>,  on accepte toujours le nouvel ordonnancement trouvé (diversification).

- <u>ILS Hillclimbing Best Improvement accept only Improving </u>: sera abbrévié en __ILS Hil B accept Betr__. Pour la recherche locale de base, c'est Hillclimbing <strong>Best improvement</strong>,  on accepte seulement un nouvel ordonnancement trouvé s'il donne un meilleur score que l'original ou celui trouvé avant lui (intensification).

<br></br>

- <u>ILS VND First Improvement accept always</u> : sera abbrévié en __ILS VND F accept Alwa__. Pour la recherche locale de base, c'est VND First improvement,
  on accepte toujours le nouvel ordonnancement trouvé (diversification).

- <u>ILS VND First Improvement accept only Improving</u> : sera abbrévié en __ILS VND F accept Betr__. Pour la recherche locale de base, c'est VND First improvement,
  on accepte seulement un nouvel ordonnancement trouvé s'il donne un meilleur score que l'original ou celui trouvé avant lui (intensification).  

- <u>ILS VND Best Improvement accept always</u> : sera abbrévié en __ILS VND B accept Alwa__. Pour la recherche locale de base, c'est VND <strong>Best improvement</strong>,
  on accepte toujours le nouvel ordonnancement trouvé (diversification).

- <u>ILS VND Best Improvement accept only Improving</u> : sera abbrévié en __ILS VND B accept Betr__. Pour la recherche locale de base, c'est VND <strong>Best improvement</strong>, on accepte seulement un nouvel ordonnancement trouvé s'il donne un meilleur score que l'original ou celui trouvé avant lui (intensification).

<br></br>
Nous estimons notre perturbation correcte, nous pouvons évidemment trouver de bons résultats avec d'autres valeurs que nombre de tâches * 0.1 inversions.  
Cependant, si nous diminuons trop la perturbation, nos recherches locales risquent de simplement revenir sur leur pas, et si nous faisons une perturbation trop élevée, cela revient à recommencer depuis le début à chaque fois, car le résultat est trop proche de l'aléatoire.  
<br></br>

### __Q1.6) Parmi les heuristiques développées, tester la variante qui vous semble être la plus adaptée et reporter vos résultats (meilleur valeur objectif, temps de calcul, etc) pour quelques instances (selon le temps de calcul dont vous disposez).__  

<br></br>


Nous enregistrons ici quelques données pour pouvoir faire des comparaisons et tirer des conclusions.  


| Heuristique / fichier | n100_15_b.txt 	| n100_19_b.txt 	| n100_39_b.txt 	| n100_89_b.txt 	| n1000_1_b.txt 	| n1000_2_b.txt 	|
|---------------------	|---------------	|---------------	|---------------	|---------------	|---------------	|---------------	|
| Aléatoire             | 483609            | 924136            | 490256            | 466351            | 3304938296        | 3207919143        |
| Heuristique 4       	| 288895           	| 783591           	| 250451           	| 126366           	| 1131909814      	| 1190825341       	|
| HillClimbing First Imp| 189902           	| 492636           	| 174987           	| 77914           	| 752848960      	| 783108505       	|
| ILS Hil F accept Alwa | 335722           	| 624819           	| 302500           	| 221614           	| 1079514409      	| 1137800753       	|
| ILS Hil F accept Betr | 189826           	| 489738           	| 159712           	| 57756           	| 744811208      	| 772287525       	|
| HillClimbing          | 197974           	| 498938           	| 181557           	| 81130           	| 770615224      	| 851554125       	|
| ILS Hil B accept Alwa | 344722           	| 582326           	| 288849           	| 186406           	| 943436706       	| 1014159625       	|
| ILS Hil B accept Betr | 189958           	| 485782           	| 160017           	| 56584           	| 748768881      	| 786476289       	|
| VND First Improvement	| 173564           	| 481024           	| 153898           	| 56522           	| 666244661      	| 688192107       	|
| ILS VND F accept Alwa	| 175024           	| 477891           	| 152775           	| 55560           	| 663666204      	| 684742812       	|
| ILS VND F accept Betr	| 173045           	| 479580           	| 152096           	| 54612           	| 665417691      	| 685167170       	|
| VND                  	| 173564           	| 479886           	| 153898           	| 56522           	| 667520216      	| 688303787       	|
| ILS VND B accept Alwa | 175114           	| 478306           	| 152541           	| 55560           	| 666638349      	| 687909639       	|
| ILS VND B accept Betr | 173045           	| 479886           	| 152096           	| 54612           	| 667520216      	| 687213827       	|
<br></br>

### Légère remarque pré-conclusion :

Un défaut des always accept est qu'on risque fortement de trouver un meilleur résultat pendant le processus que celui renvoyé à la fin.
<br></br>

### <span style="color:#ff2288">__Réponse exacte à 1.6 ) :__</span>
<br></br>
De manière générale les ILS HillClimbing sont moins performantes que les ILS VND, cependant nous pouvons tout de même constater que les ILS Hill Climbing avec le type de mouvement "seulement accepter les améliorants" offrent des scores proches d'un VND non ILS.  
Le mouvement "toujours accepter" est contre-productif pour les ILS HillClimbing.  
Notre remarque pré-conclusions est pertinente dans le cas des ILS Hillclimbing, vous verrez que ce n'est pas le cas pour les ILS VND.

<br></br>

De manière générale les ILS VND sont très performantes, les résultats obtenus sont soit l'optimum global ou alors de très bons optimums locaux. Les résultats observés avec nos deux critères d'acceptation First et Best improvement sont sembables.
On remarque également que les réusltats donnés les types de mouvement "toujours accepter" et "seulement les améliorants" sont sont similaires pour les listes de grandes tailles, bien que nous voyons un léger avantage pour l'ILS VND First Improvement accept Always.
Les VND font une plus grande recherche de voisinage, ce qui leur permet de mieux repartir depuis les perturbations causées par un "toujours accepter", et avoir accès à une plus grande diversification.
<br></br>


<span style="color:#ff0022">D'après nos observations la variante la plus adaptée est *l'ILS VND First Improvement accept Always*, cette variante se démarque notamment des autres ILS VND grâce a ses meilleurs résultats sur les jeux de données de taille 1000.</span>  
<br></br>


| Heuristique / fichier | n100_15_b.txt 	| n100_19_b.txt 	| n100_39_b.txt 	| n100_89_b.txt 	| n1000_1_b.txt 	| n1000_2_b.txt 	|
|---------------------	|---------------	|---------------	|---------------	|---------------	|---------------	|---------------	|
| ILS VND F accept Alwa	| 175024           	| 477891           	| 152775           	| 55560           	| 663666204      	| 684742812       	|
<br></br>


Nous avons constaté une amélioration déja entre l'Aléatoire et l'heuristique 4, qui est parfois 15% plus efficace, et d'autre fois fois 2 ou 3 fois plus efficace que l'aléatoire (voir Q1.3), mais les VND peuvent être plus de 2 fois plus efficaces que l'heuristique 4, au pire ils offrent un score d'ordonnancement 40 % meilleur. Les ILS VND soit font pareil que VND, soit offrent un résultat légèrement meilleur, souvent moins d'1 % meilleur, mais c'est non négligeable car cela peut mener à trouver l'ordonnancement optimal.  
<br></br>
Nous ne pouvons cependant pas négliger le fait qu'une solution de meilleur qualité est trouvée au détriment du temps de calcul, qui augmente de manière exponentielle.  

Souvenons nous qu'il fallait par exemple 0.05 secondes pour arriver à une solution pour n = 100, à 100 secondes pour n = 1000 avec un VND First improvement...
<br></br>
### __Q1.7) Soumettez la meilleur solution trouvée pour chaque instance sur la plateforme de test.__
Fait :
![Image contest-vnd.png](./contest-vnd.png)
<br></br>

## __*Bonus*__
<br></br>

### __Q1.8) Proposer et tester n'importe quel autre type d'algorithme qui vous semble plus efficace. Reportez vos meilleures solutions pour les différentes instances de la plateforme de test.__  
<br></br>

Nous proposons et avons testé un algorithme de recuit simulé, nous avons choisi une temperature ayant pour valeur 16 à l'origine, on choisit simplement un voisin aléatoire sauf quand la température baisse à 2 et moins, dans ce cas nous faisons un VND en Best improvement. (Nous soupçonnons de meilleurs performances lorsque nous travaillons avec des puissances de 2, comme 16 et 2)  
Le premier VND de l'algorithme, celui pour démarrer à partir d'une solution intiale,
est un VND en First Improvement, car il est plus rapide que le Best. Le voisinage est donné par swap, il y a donc n * (n-1) / 2 voisins à une solution.

Il s'agit de la __fonction recuit_simule() ligne 599__.  


| Heuristique / fichier | n100_15_b.txt 	| n100_19_b.txt 	| n100_39_b.txt 	| n100_89_b.txt 	| n1000_1_b.txt 	| n1000_2_b.txt 	|
|---------------------	|---------------	|---------------	|---------------	|---------------	|---------------	|---------------	|
| ILS VND F accept Alwa	| 175024           	| 477891           	| 152775           	| 55560           	| 663666204      	| 684742812       	|
| ILS VND F accept Betr	| 173045           	| 479580           	| 152096           	| 54612           	| 665417691      	| 685167170       	|
| recuit simulé         | 172995            | 477684            | 151701            | 54612             | 662177536         | 684963977         |
<br></br>

Les solutions trouvées sont sont encore meilleures que l'ILS_VND ou égales dans le cas où l'ILS VND a déja trouvé la solution optimale pour n = 100.
Quand n = 1000, les résultats sont similaires.  
<br></br>


Nous avons constaté une amélioration déja entre l'Aléatoire et l'heuristique 4, qui est parfois 15% plus efficace, et d'autre fois fois 2 ou 3 fois plus efficace que l'aléatoire (voir Q1.3), mais les VND peuvent être plus de 2 fois plus efficaces que l'heuristique 4, au pire ils offrent un score d'ordonnancement 40 % meilleur.  
<br></br>

Les ILS VND soit font pareil que VND, soit offre un résultat légèrement meilleur, souvent moins d'1 % meilleur, mais c'est déja ça, et cela peut mener à trouver l'ordonnancement optimal. Le recuit simulé, est encore meilleur que nos ILS VND en ce qu'il trouve toujours un résultat égal ou meilleur qu'eux, et trouve plus souvent l'optimal (si ce n'est pas toujours le cas quand n = 100).  

On peut noter qu'à partir de nos algorithmes de VND, nos ratios de garanties se rapprochent serieusement de 1.

Pour permettre à notre recuit simulé de travailler avec de plus grandes données, par exemple n = 10 000, nous pourrions retirer les VND et les remplacer par des Hillclimbing et utiliser des threads (puis convertir certaines variables int en long).