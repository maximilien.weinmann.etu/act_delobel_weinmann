public class Task {
    public int execution_time;
    public int weight;
    public int time_limit;
    public int original_index;
    public long retard;

    public Task(int execution,int weight,int time_limit, int original_index) {
        this.execution_time = execution;
        this.weight = weight;
        this.time_limit = time_limit;
        this.original_index = original_index;
        this.retard = 0;
    }

    public int getWeight() {
        return this.weight;
    }

    public int getTime_Limit() {
        return this.time_limit;
    }

    public int getWeightTimeLimit_ratio() {
        return this.weight / this.time_limit;
    }

    public long getRetard() {
        return this.retard;
    }

    public void setRetard(long retard) {
        this.retard = retard;
    }

    public String toString() {
        return this.execution_time+" "+this.weight+" "+this.time_limit;
    }
}