import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.ArrayList;

public class Main { 

    private static Random rand = new Random();
    
    /*
     * Une heuristique constructive génère une solution,
     * laquelle est vide au commencement (liste vide, dictionnaire vide etc)
     * en ajoutant morceau par morceau à la solution jusqu'à satisfaction.
     * L'ajout d'un morceau est définitif et le morceau est immuable.
     * 
     * 
     * Ordonner par plus grand retard, puis par poids
     */
    public static int[] heuristiqueConstructive1(ArrayList<Task> ourTasks) {
        // On crée une liste de taille n
        int[] res = new int[ourTasks.size()];
        int res_index = 0;
        // on créé une copie de notre liste de taches
        ArrayList<Task> notUsedTasks = new ArrayList<Task>();
        for (Task task : ourTasks) {
            notUsedTasks.add(new Task(task.execution_time, task.weight, task.time_limit, task.original_index));
        }

        // On regarde qui serait potentiellement le plus en retard parmi les taches non rangées
        int timeSinceStart = 0;

        while(notUsedTasks.size() > 1) {

            for (Task task : notUsedTasks) {
                long retard = Math.max((task.execution_time + timeSinceStart) - task.time_limit, 0);
                task.setRetard(retard);
            }
            
            // On range les retardataires par ordre croissant de retard
            notUsedTasks.sort(Comparator.comparing(Task::getRetard));

            //S'il n'y a pas d'égalité
            if (notUsedTasks.get(notUsedTasks.size() - 1).retard > notUsedTasks.get(notUsedTasks.size() - 2).retard) {
                res[res_index] = notUsedTasks.get(notUsedTasks.size() - 1).original_index;
                notUsedTasks.remove(notUsedTasks.get(notUsedTasks.size() - 1));
            } else {
                // On regarde quelles sont les tasks les plus en retard et en égalité
                ArrayList<Task> les_plus_en_retard = new ArrayList<Task>();
                for (Task task : notUsedTasks) {
                    if (task.retard == notUsedTasks.get(notUsedTasks.size() - 1).retard) {
                        les_plus_en_retard.add(task);
                    }
                }
                les_plus_en_retard.sort(Comparator.comparing(Task::getWeight));
                res[res_index] = les_plus_en_retard.get(les_plus_en_retard.size() - 1).original_index;
                notUsedTasks.remove(les_plus_en_retard.get(les_plus_en_retard.size() - 1));
            }
            timeSinceStart += ourTasks.get(res[res_index]).execution_time;
            res_index++;
        }
        res[res_index] = notUsedTasks.get(notUsedTasks.size() - 1).original_index;
        return res;
    }

    // retard puis poids puis date limite
    public static int[] heuristiqueConstructive2(ArrayList<Task> ourTasks) {
        int[] res = new int[ourTasks.size()];
        int res_index = 0;
        ArrayList<Task> notUsedTasks = new ArrayList<Task>();
        for (Task task : ourTasks) {
            notUsedTasks.add(new Task(task.execution_time, task.weight, task.time_limit, task.original_index));
        }

        int timeSinceStart = 0;

        while(notUsedTasks.size() > 1) {
            for (Task task : notUsedTasks) {
                long retard = Math.max((task.execution_time + timeSinceStart) - task.time_limit, 0);
                task.setRetard(retard);
            }
            
            notUsedTasks.sort(Comparator.comparing(Task::getRetard));

            if (notUsedTasks.get(notUsedTasks.size() - 1).retard > notUsedTasks.get(notUsedTasks.size() - 2).retard) {
                res[res_index] = notUsedTasks.get(notUsedTasks.size() - 1).original_index;
                notUsedTasks.remove(notUsedTasks.get(notUsedTasks.size() - 1));
            } else {
                ArrayList<Task> les_plus_en_retard = new ArrayList<Task>();
                for (Task task : notUsedTasks) {
                    if (task.retard == notUsedTasks.get(notUsedTasks.size() - 1).retard) {
                        les_plus_en_retard.add(task);
                    }
                }
                les_plus_en_retard.sort(Comparator.comparing(Task::getWeight));

                if (les_plus_en_retard.get(les_plus_en_retard.size() - 1).weight > les_plus_en_retard.get(les_plus_en_retard.size() - 2).weight) {
                    res[res_index] = les_plus_en_retard.get(les_plus_en_retard.size() - 1).original_index;
                    notUsedTasks.remove(les_plus_en_retard.get(les_plus_en_retard.size() - 1));
                } else {
                    ArrayList<Task> les_plus_lourds = new ArrayList<Task>();
                    for (Task task : les_plus_en_retard) {
                        if (task.weight == les_plus_en_retard.get(les_plus_en_retard.size() - 1).weight) {
                            les_plus_lourds.add(task);
                        }
                    }
                    les_plus_lourds.sort(Comparator.comparing(Task::getTime_Limit));
                    res[res_index] = les_plus_lourds.get(les_plus_lourds.size() - 1).original_index;
                    notUsedTasks.remove(les_plus_lourds.get(les_plus_lourds.size() - 1));
                }
            }
            timeSinceStart += ourTasks.get(res[res_index]).execution_time;
            res_index++;
        }
        res[res_index] = notUsedTasks.get(notUsedTasks.size() - 1).original_index;
        return res;
    }

    // Retard puis ration poids/temps limite
    public static int[] heuristiqueConstructive3(ArrayList<Task> ourTasks) {
        int[] res = new int[ourTasks.size()];
        int res_index = 0;
        ArrayList<Task> notUsedTasks = new ArrayList<Task>();
        for (Task task : ourTasks) {
            notUsedTasks.add(new Task(task.execution_time, task.weight, task.time_limit, task.original_index));
        }

        int timeSinceStart = 0;

        while(notUsedTasks.size() > 1) {
            for (Task task : notUsedTasks) {
                long retard = Math.max((task.execution_time + timeSinceStart) - task.time_limit, 0);
                task.setRetard(retard);
            }
            
            notUsedTasks.sort(Comparator.comparing(Task::getRetard));

            if (notUsedTasks.get(notUsedTasks.size() - 1).retard > notUsedTasks.get(notUsedTasks.size() - 2).retard) {
                res[res_index] = notUsedTasks.get(notUsedTasks.size() - 1).original_index;
                notUsedTasks.remove(notUsedTasks.get(notUsedTasks.size() - 1));
            } else {
                ArrayList<Task> les_plus_en_retard = new ArrayList<Task>();
                for (Task task : notUsedTasks) {
                    if (task.retard == notUsedTasks.get(notUsedTasks.size() - 1).retard) {
                        les_plus_en_retard.add(task);
                    }
                }
                les_plus_en_retard.sort(Comparator.comparing(Task::getWeightTimeLimit_ratio));
                res[res_index] = les_plus_en_retard.get(0).original_index;
                notUsedTasks.remove(les_plus_en_retard.get(0));
            }
            timeSinceStart += ourTasks.get(res[res_index]).execution_time;
            res_index++;
        }
        res[res_index] = notUsedTasks.get(notUsedTasks.size() - 1).original_index;
        return res;
    }

    // date limite, puis poids, et enfin prise en compte du retard
    public static int[] heuristiqueConstructive4(ArrayList<Task> ourTasks) {
        int[] res = new int[ourTasks.size()];
        int res_index = 0;
        ArrayList<Task> notUsedTasks = new ArrayList<Task>();
        for (Task task : ourTasks) {
            notUsedTasks.add(new Task(task.execution_time, task.weight, task.time_limit, task.original_index));
        }

        int timeSinceStart = 0;

        while(notUsedTasks.size() > 1) {
            for (Task task : notUsedTasks) {
                long retard = Math.max((task.execution_time + timeSinceStart) - task.time_limit, 0);
                task.setRetard(retard);
            }
            
            notUsedTasks.sort(Comparator.comparing(Task::getTime_Limit));
            List<Task> les_plus_petites_date_limite = new ArrayList<Task>();     

            if (notUsedTasks.size() > (int) Math.round(0.2 * ourTasks.size()) - 1) {
                les_plus_petites_date_limite = notUsedTasks.subList(0, (int) Math.round(0.2 * ourTasks.size()));
            } else {
                les_plus_petites_date_limite = notUsedTasks.subList(0, notUsedTasks.size());
            }            // on va faire une comparaison de poids parmi les 20 plus petits temps limite
            les_plus_petites_date_limite.sort(Comparator.comparing(Task::getWeight));

            // si pas d'égalité de poids, on prend le plus lourd
            if (les_plus_petites_date_limite.get(les_plus_petites_date_limite.size() - 1).weight > les_plus_petites_date_limite.get(les_plus_petites_date_limite.size() - 2).weight) {
                res[res_index] = les_plus_petites_date_limite.get(les_plus_petites_date_limite.size() - 1).original_index;
                notUsedTasks.remove(les_plus_petites_date_limite.get(les_plus_petites_date_limite.size() - 1));
            } else {
                // On regarde quelles sont les tasks les plus en retard parmi les plus lourdes
                ArrayList<Task> les_plus_en_retard = new ArrayList<Task>();
                for (Task task : les_plus_petites_date_limite) {
                    if (task.weight == les_plus_petites_date_limite.get(les_plus_petites_date_limite.size() - 1).weight) {
                        les_plus_en_retard.add(task);
                    }
                }
                les_plus_en_retard.sort(Comparator.comparing(Task::getRetard));
                res[res_index] = les_plus_en_retard.get(0).original_index;
                notUsedTasks.remove(les_plus_en_retard.get(0));
            }
            timeSinceStart += ourTasks.get(res[res_index]).execution_time;
            res_index++;
        }
        res[res_index] = notUsedTasks.get(notUsedTasks.size() - 1).original_index;
        return res;
    }

    public static int[] inversion(int[] originale, int[] solution) {
        if (solution[solution.length - 1] == originale[solution.length - 2]) {
            int temp = solution[solution.length - 1];
            solution[solution.length - 1] = solution[0];
            solution[0] = solution[solution.length - 2];
            solution[solution.length - 2] = temp;
            return solution;
        }
        for (int i = 0; i < solution.length - 2; i++) {
            if (solution[i] != originale[i]) {
                int temp = solution[i];
                solution[i] = solution[i + 1];
                solution[i + 1] = solution[i + 2];
                solution[i + 2] = temp;
                break;
            }
        }
        return solution;
    }

    public static int[] ordonnancementFromIndice(int[] originale, int indice) {
        if (indice == originale.length - 1) {
            int temp = originale[originale.length - 1];
            originale[originale.length - 1] = originale[0];
            originale[0] = temp;
        } else {
            int temp = originale[indice];
            originale[indice] = originale[indice + 1];
            originale[indice + 1] = temp;
        }
        return originale;
    }

    // le principe est de partir d'un ordonnancement, de regarder parmi ses voisins s'il y en a un meilleur,(best fit ou first fit)
    // si oui le choisir, regarder parmi ses voisins etc
    public static int[] HillClimbing(ArrayList<Task> ourTasks, int[] perturbation, String move) {
        int[] solutionOriginale = new int[ourTasks.size()];
        int indexcopy = 0;
        if (perturbation != null) {
            for (int i : perturbation) {
                solutionOriginale[indexcopy] = i;
                indexcopy++;
            }
        } else {
            solutionOriginale = heuristiqueConstructive4(ourTasks);
        }
        int[] solutionVoisine = new int[solutionOriginale.length];
        if (move == "Best") {       // Best Improvement
            long[] score = new long[solutionOriginale.length];
            long[] bestScore = new long[2];
            while (true) {
                long OG_score = soluceQuality(solutionOriginale, ourTasks);
                int index = 0;
                for (int i : solutionOriginale) {
                    solutionVoisine[index] = i;
                    index++;
                }
                int temp = solutionVoisine[0];
                solutionVoisine[0] = solutionVoisine[1];
                solutionVoisine[1] = temp;
                score[0] = soluceQuality(solutionVoisine, ourTasks);// l'indice du score est le voisin

                bestScore[0] = score[0];
                bestScore[1] = 0;
                for (int i = 1; i < solutionOriginale.length; i++) {
                    solutionVoisine = inversion(solutionOriginale, solutionVoisine);
                    long voisinScore = soluceQuality(solutionVoisine, ourTasks);
                    score[i] = voisinScore;
                    if (score[i] < bestScore[0]) { // attention l'objectif est que le score soit le plus petit possible
                        bestScore[0] = score[i];
                        bestScore[1] = i;
                    }
                }
                
                if (OG_score <= bestScore[0]) {
                    return solutionOriginale;
                }
                
                ordonnancementFromIndice(solutionOriginale, (int) bestScore[1]);
            }
        } else if (move == "First") {
            long score;
            boolean continue_while = true;
            while (continue_while) {
                long OG_score = soluceQuality(solutionOriginale, ourTasks);
                int index = 0;
                for (int i : solutionOriginale) {
                    solutionVoisine[index] = i;
                    index++;
                }
                int temp = solutionVoisine[0];
                solutionVoisine[0] = solutionVoisine[1];
                solutionVoisine[1] = temp;
                score = soluceQuality(solutionVoisine, ourTasks);// l'indice du score est le voisin
                if (score < OG_score) {
                    ordonnancementFromIndice(solutionOriginale, 0);
                    continue;
                }
                continue_while = false;
                for (int i = 1; i < solutionOriginale.length; i++) {
                    solutionVoisine = inversion(solutionOriginale, solutionVoisine);
                    long voisinScore = soluceQuality(solutionVoisine, ourTasks);
                    if (voisinScore < OG_score) {
                        ordonnancementFromIndice(solutionOriginale, i);
                        continue_while = true;
                        break;
                    }
                }
            }
            return solutionOriginale;
        } else {
            long[] score = new long[solutionOriginale.length];
            long[] worseScore = new long[2];
            while (true) {
                long OG_score = soluceQuality(solutionOriginale, ourTasks);
                int index = 0;
                for (int i : solutionOriginale) {
                    solutionVoisine[index] = i;
                    index++;
                }
                int temp = solutionVoisine[0];
                solutionVoisine[0] = solutionVoisine[1];
                solutionVoisine[1] = temp;
                worseScore[0] = -1;
                worseScore[1] = -1;
                for (int i = 1; i < solutionOriginale.length; i++) {
                    solutionVoisine = inversion(solutionOriginale, solutionVoisine);
                    long voisinScore = soluceQuality(solutionVoisine, ourTasks);
                    score[i] = voisinScore;
                    if ((score[i] > worseScore[0]) && (score[i] < OG_score)) { // attention l'objectif est que le score soit le plus grand possible mais toujours améliorant ou égal
                        worseScore[0] = score[i];
                        worseScore[1] = i;
                    }
                }
                
                if (worseScore[0] == -1) {
                    return solutionOriginale;
                }
                
                ordonnancementFromIndice(solutionOriginale, (int) worseScore[1]);
            }
        }
    }

    public static int[] voisin(int shift, int distance_inversion, int[] originale) {
        int[] voisin = originale.clone();
        int taille = originale.length;
        int temp = voisin[(shift + distance_inversion) % taille];
        voisin[(shift + distance_inversion) % taille] = voisin[shift];
        voisin[shift] = temp;
        return voisin;
    }

    public static int[] meilleur_voisin_du_voisinage(int distance_inversion, int[] originale, ArrayList<Task> ourTasks) {
        int[][] voisinage = new int[originale.length][originale.length];
        int taille = originale.length;
        long[] record = new long[2];
        
        for (int i = 0; i < taille; i++) {
            voisinage[i] = voisin(i, distance_inversion, originale);
        }

        record[1] = 0;
        record[0] = soluceQuality(voisinage[0], ourTasks);
        int index = 0;
        for (int[] voisin : voisinage) {
            long score = soluceQuality(voisin, ourTasks);
            if (score < record[0]) {
                record[0] = score;
                record[1] = index;
            }
            index++;
        }

        return voisinage[(int) record[1]];
    }

    public static int[] premier_meilleur_voisin_du_voisinage(int distance_inversion, int[] originale, ArrayList<Task> ourTasks, long best_score) {
        int[] premier_meilleur_voisin = new int[originale.length];
        int taille = originale.length;
        
        for (int i = 0; i < taille; i++) {
            premier_meilleur_voisin = voisin(i, distance_inversion, originale);
            if (soluceQuality(premier_meilleur_voisin, ourTasks) < best_score) {
                return premier_meilleur_voisin;
            }
        }
        return originale;
    }

    public static int[] VND(ArrayList<Task> ourTasks, int[] perturbateur, String move) {
        int[] solutionOriginale = new int[ourTasks.size()];
        if (perturbateur == null) {
            solutionOriginale = heuristiqueConstructive4(ourTasks);
        } else {
            solutionOriginale = perturbateur;
        }
        int[] meilleur_ordonnancement = solutionOriginale.clone();
        long best_score = soluceQuality(solutionOriginale, ourTasks);
        int indice_voisinage = 1; // sert aussi de distance pour les inversion
        int nb_voisinage = ourTasks.size() - 1;
        if (move == "Best") {
            while (indice_voisinage <= nb_voisinage) {
                int[] meilleur_voisin_du_voisinage = meilleur_voisin_du_voisinage(indice_voisinage, meilleur_ordonnancement, ourTasks);
                long score = soluceQuality(meilleur_voisin_du_voisinage, ourTasks);
                
                if (score < best_score) {
                    best_score = score;
                    meilleur_ordonnancement = meilleur_voisin_du_voisinage;
                    indice_voisinage = 1;
                } else {
                    indice_voisinage++;
                }
            }
        } else {
            while (indice_voisinage <= nb_voisinage) {
                int[] premier_meilleur_voisin_du_voisinage = premier_meilleur_voisin_du_voisinage(indice_voisinage, meilleur_ordonnancement, ourTasks, best_score);
                long score = soluceQuality(premier_meilleur_voisin_du_voisinage, ourTasks);
                
                if (score < best_score) {
                    best_score = score;
                    meilleur_ordonnancement = premier_meilleur_voisin_du_voisinage;
                    indice_voisinage = 1;
                } else {
                    indice_voisinage++;
                }
            }
        }
        return meilleur_ordonnancement;
    }

    // il s'agit d'inversions aléatoires
    public static int[] perturbateur(int[] sEtoile, int k) {
        int[] perturbed = new int[sEtoile.length];
        int index = 0;
        for (int i : sEtoile) {
            perturbed[index] = i;
            index++;
        }
        int indexPerturbed;
        int temp;
        int round = k;
        while (round > 0) {
            indexPerturbed = rand.nextInt(perturbed.length);
            temp = perturbed[indexPerturbed];
            if (indexPerturbed < perturbed.length - 1) {
                perturbed[indexPerturbed] = perturbed[indexPerturbed + 1];
                perturbed[indexPerturbed + 1] = temp;
            } else {
                perturbed[indexPerturbed] = perturbed[0];
                perturbed[0] = temp;
            }
            round--;
        }
        return perturbed;
    }


    public static int[] Accept_onlyBetter(int[] sEtoile, int[] perturbationEtoile, long sEtoileScore, long perturbationScore, ArrayList<Task> ourTasks) {
        if (perturbationScore < sEtoileScore) {
            int index = 0;
            for (int i : perturbationEtoile) {
                sEtoile[index] = i;
                index++;
            }
        }
        return sEtoile;
    }

    public static int[] Accept_All(int[] sEtoile, int[] perturbationEtoile, ArrayList<Task> ourTasks) {
        int index = 0;
        for (int i : perturbationEtoile) {
            sEtoile[index] = i;
            index++;
        }
        return sEtoile;
    }

    public static int[] ILS_Hillclimbing(ArrayList<Task> ourTasks, int k, String move, String accept) {
        long start = System.nanoTime();
        int[] sEtoile = HillClimbing(ourTasks, null, move);
        long sEtoileScore = soluceQuality(sEtoile, ourTasks);
        long time_spent = start;
        double max_time = ourTasks.size() * 0.6 * 1000_000_000;
        if (accept == "always") {
            while ((time_spent - start) < max_time) { // 30 secondes si 100 taches, 5 minutes si 1000
                int[] perturbation = perturbateur(sEtoile, k);
                int[] perturbationEtoile = HillClimbing(ourTasks, perturbation, move);
                sEtoile = Accept_All(sEtoile, perturbationEtoile, ourTasks);
                time_spent = System.nanoTime();
            }
        } else if (accept == "onlyBetter") {
            while ((time_spent - start) < max_time) {
                int[] perturbation = perturbateur(sEtoile, k);
                int[] perturbationEtoile = HillClimbing(ourTasks, perturbation, move);
                long perturbationScore = soluceQuality(perturbationEtoile, ourTasks);
                sEtoile = Accept_onlyBetter(sEtoile, perturbationEtoile, sEtoileScore, perturbationScore, ourTasks);
                if (perturbationScore < sEtoileScore) {
                    sEtoileScore = perturbationScore;
                }
                time_spent = System.nanoTime();
            }
        }
        return sEtoile;
    }

    public static int[] ILS_VND(ArrayList<Task> ourTasks, int k, String move, String accept) {
        long start = System.nanoTime();
        
        int[] sEtoile = VND(ourTasks, null, move);
        long sEtoileScore = soluceQuality(sEtoile, ourTasks);
        long time_spent = start;
        double max_time = ourTasks.size() * 0.6 * 1000_000_000;
        if (accept == "always") {
            while((time_spent - start) < max_time) {
                int[] perturbation = perturbateur(sEtoile, k);
                int[] perturbationEtoile = VND(ourTasks, perturbation, move);
                sEtoile = Accept_All(sEtoile, perturbationEtoile, ourTasks);
                time_spent = System.nanoTime();
            }
        } else if (accept == "onlyBetter") {
            while((time_spent - start) < max_time) {
                int[] perturbation = perturbateur(sEtoile, k);
                int[] perturbationEtoile = VND(ourTasks, perturbation, move);
                long perturbationScore = soluceQuality(perturbationEtoile, ourTasks);
                sEtoile = Accept_onlyBetter(sEtoile, perturbationEtoile, sEtoileScore, perturbationScore, ourTasks);
                if (perturbationScore < sEtoileScore) {
                    sEtoileScore = perturbationScore;
                }
                time_spent = System.nanoTime();
            }
        }
        return sEtoile;
    }

    // Q1.1)
    public static long soluceQuality(int[] Ordonnancement, ArrayList<Task> ourTasks) {
        long total = 0;
        long timeSinceStart = 0;
        for (int task_index : Ordonnancement) {
            total += (long) ourTasks.get(task_index).weight * Math.max(((long) ourTasks.get(task_index).execution_time + timeSinceStart) - (long) ourTasks.get(task_index).time_limit, 0);
            timeSinceStart += ourTasks.get(task_index).execution_time;
        }
        return total;
    }

    /**
     * q1.2)
     */
    public static int[] generateRandomSoluce(ArrayList<Task> data) {
        int [] soluce = new int [data.size()];
        for (int i = 0; i < data.size(); i++) {
            soluce[i] = i;
        }
        for (int i = 0; i < data.size(); i++) {
            int randomIndexToSwap = rand.nextInt(soluce.length);
            int temp = soluce[randomIndexToSwap];
            soluce[randomIndexToSwap] = soluce[i];
            soluce[i] = temp;
        }
        return soluce;
    }


    public static ArrayList<Task> parseFile(String filename) throws IOException {
        FileReader reader = new FileReader(filename);
        BufferedReader bfr = new BufferedReader(reader);
        int n = Integer.parseInt(bfr.readLine());
        ArrayList<Task> values = new ArrayList<Task>();
        for (int i = 0; i < n; i++) {
            String [] line = bfr.readLine().split(" ");
            values.add(new Task(Integer.parseInt(line[0]), Integer.parseInt(line[1]), Integer.parseInt(line[2]), i));
        }
        bfr.close();
        return values;
    }


    public static int[] voisin_swap(int[] solutionOriginale) {
        int[] voisin = solutionOriginale.clone();
        int indextoSwap1 = rand.nextInt(voisin.length);
        int indextoSwap2 = rand.nextInt(voisin.length);
        int temp = voisin[indextoSwap1];
        voisin[indextoSwap1] = voisin[indextoSwap2];
        voisin[indextoSwap2] = temp;
        return voisin;
    }

    public static int[] recuit_simule(ArrayList<Task> ourTasks) {
        long start = System.nanoTime();
        long time_spent = start;
        double max_time = ourTasks.size() * 0.6 * 1000_000_000;

        // une solution initiale
        int taille_tasks = ourTasks.size();
        int nb_swap = (taille_tasks * (taille_tasks - 1)) >> 1;
        int[] solutionInitiale = VND(ourTasks, heuristiqueConstructive4(ourTasks), "First");

        // une temperature initiale
        double T0 = 16;
        double temperature = T0;
        int i = 0;
        while ((time_spent - start) < max_time) {
            // meta-iteration à temperature fixe
            int count = 0;
            while (count < nb_swap) {
                int[] solutionVoisine = new int[taille_tasks];
                if (temperature > 2) {
                    solutionVoisine = voisin_swap(solutionInitiale);
                } else {
                    solutionVoisine = VND(ourTasks, voisin_swap(solutionInitiale), "Best");
                }
                long delta = soluceQuality(solutionInitiale, ourTasks) - soluceQuality(solutionVoisine, ourTasks);
                if (delta >= 0) {
                    solutionInitiale = solutionVoisine.clone();
                } else {
                    if (rand.nextDouble() < Math.exp(delta / temperature)) {
                        solutionInitiale = solutionVoisine.clone();
                    }
                }
                count++;
            }
            i++;
            // cooling
            temperature = T0 / Math.log(i);
            time_spent = System.nanoTime();
        }
        return solutionInitiale;
    }


    public static void main(String[] args) throws IOException {
        ArrayList<Task> tasks = Main.parseFile(args[0]);

        // Q1.1 et Q1.2
        System.out.println("Durée de génération en seconde et résultat évaluation solution aléatoire : ");
        long start = System.nanoTime();
        int[] une_solution = generateRandomSoluce(tasks);
        long finish = System.nanoTime();
        long duree = finish - start;
        System.out.print("Durée : ");
        System.out.print((double) ((double)duree / 1000_000_000));
        System.out.println(" s");
        System.out.print("Évaluation : ");
        System.out.println(soluceQuality(une_solution, tasks));
        System.out.println("\n");

        // Q1.3
        System.out.println("Durée de génération en seconde et résultat évaluation heuristique constructive 4 : ");
        start = System.nanoTime();
        une_solution = heuristiqueConstructive4(tasks);
        finish = System.nanoTime();
        duree = finish - start;
        System.out.print("Durée : ");
        System.out.print((double) ((double)duree / 1000_000_000));
        System.out.println(" s");
        System.out.print("Évaluation : ");
        System.out.println(soluceQuality(une_solution, tasks));
        System.out.println("\n");

        // Q1.4
        System.out.println("Durée de génération en seconde et résultat évaluation Hillclimbing : ");
        start = System.nanoTime();
        une_solution = HillClimbing(tasks, null, "Worse"); // 3e argument : "Best" ou "First" ou "Worse"
        finish = System.nanoTime();
        duree = finish - start;
        System.out.print("Durée : ");
        System.out.print((double) ((double)duree / 1000_000_000));
        System.out.println(" s");
        System.out.print("Évaluation : ");
        System.out.println(soluceQuality(une_solution, tasks));
        System.out.println();

        System.out.println("Durée de génération en seconde et résultat évaluation VND : ");
        start = System.nanoTime();
        une_solution = VND(tasks, null, "Best"); // 3e argument : "Best" ou "First"
        finish = System.nanoTime();
        duree = finish - start;
        System.out.print("Durée : ");
        System.out.print((double) ((double)duree / 1000_000_000));
        System.out.println(" s");
        System.out.print("Évaluation : ");
        System.out.println(soluceQuality(une_solution, tasks));
        System.out.println();

        // Q1.5
        System.out.println("Durée de génération en seconde et résultat évaluation ILS_Hillclimbing : ");
        start = System.nanoTime();
        une_solution = ILS_Hillclimbing(tasks, (int) (tasks.size() * 0.1), "Best", "onlyBetter"); // 3e argument : "Best" ou "First" ou "Worse", 4e argument : "always" ou "onlyBetter"
        finish = System.nanoTime();
        duree = finish - start;
        System.out.print("Durée : ");
        System.out.print((double) ((double)duree / 1000_000_000));
        System.out.println(" s");
        System.out.print("Évaluation : ");
        System.out.println(soluceQuality(une_solution, tasks));
        System.out.println();
        
        System.out.println("Durée de génération en seconde et résultat évaluation ILS_VND : ");
        start = System.nanoTime();
        une_solution = ILS_VND(tasks, (int) (tasks.size() * 0.1), "First", "always"); // 3e argument : "Best" ou "First", 4e argument : "always" ou "onlyBetter"
        finish = System.nanoTime();
        duree = finish - start;
        System.out.print("Durée : ");
        System.out.print((double) ((double)duree / 1000_000_000));
        System.out.println(" s");
        System.out.print("Évaluation : ");
        System.out.println(soluceQuality(une_solution, tasks));
        System.out.println();
        
        // Q1.8
        System.out.println("Durée de génération en seconde et résultat évaluation recuit simulé : ");
        start = System.nanoTime();
        une_solution = recuit_simule(tasks);
        finish = System.nanoTime();
        duree = finish - start;
        System.out.print("Durée : ");
        System.out.print((double) ((double)duree / 1000_000_000));
        System.out.println(" s");
        System.out.print("Évaluation : ");
        System.out.println(soluceQuality(une_solution, tasks));
        System.out.println();
    }
}