# Subfonction thats find the largest rectangle area drawable from a pivot(left side of the rectangle) and exploring elements of the array leftside of pivot
def LeftSideRun(w,h,plist,pivot ,minY,leftSideMax) :
    
    if len(plist) < 1 :
        return leftSideMax
    # Pour calculer à chaque fois la zone qui va de (pt[n+1] - pt[n] * h)
    last_pt = pivot
    # Value of the area represented by the histogram from the current "point" in the loop and his predecessor 
    current_skyBottomArea = 0
    # Maximum area represented by an histogram among the list elements
    max_skyBottomArea = 0

     # Highest Y value to create correct rectangles area, initialized to the Y of the first element that will be processed by the loop
    max_y = plist[-1][1]
     # Temporary Maximum Area value obtained with a "Y-reduction"
    # Meaning that a new localMax will be calculated each time the current element Y value is lower than the predecessor
    # Calcul uses the formula (currentElementX - pivotX) * max_Y
    localMax = 0

##LeftSideRun meaning that the list will be processed tail to head going from pivot-1 to first element.
    for left in range(len(plist) - 1, -1, -1) :
        
        # Current element "histogram area" is calculated, expressed : ([currentElement-1].X) - currentElement.X * h (height of the global area)
        current_skyBottomArea = (last_pt[0] - plist[left][0]) * h

        # Checking if the current "histogram" area is bigger than the current leftSideMax area
        if current_skyBottomArea > max_skyBottomArea :
            max_skyBottomArea = current_skyBottomArea

        ## Missing something here ...


        # Note : Will always enter here during the first occurence of the loop because max_y is set to the Y of the element that will be treated in first position.
        if (plist[left][1] <= max_y) :

            localMax = (pivot[0] - plist[left][0]) * max_y
            #Updating "max_y" value
            max_y = plist[left][1] 

        # Entering else statement means that current point Y is higher than the previous maximal Y obtained.
        # First idea
        else :
            # Check if the localMax calculated is larger than our actual leftSideMax(Speaking of leftSideMax due to original pivot)
            if localMax > max_skyBottomArea :
                if localMax > leftSideMax :
                    leftSideMax = localMax
            else :
                if max_skyBottomArea > leftSideMax :
                    leftSideMax = max_skyBottomArea

            # Here we are looking to save the min Y among all the rightside to create the widest area possible in the leftside
            # Note : Could be useless since the addition of the yTopLimit checking because we should normally get the minY in yTopLimit variable after going through all the list.
            if minY > last_pt[1] :
                minY = last_pt[1] 
            return LeftSideRun(w,h,plist[:plist.index(last_pt)],last_pt,minY,leftSideMax) #avec la sous liste [0,left.x] max_surface = localMax        
        
        last_pt = plist[left]
    return max([localMax,max_skyBottomArea,leftSideMax,w*minY])

#-----------------------------------------------------------------------------------------------------------------
# Subfonction thats find the largest rectangle area drawable from a pivot(right side of the rectangle) and exploring elements of the array rightside of pivot
def RightSideRun(w,h,plist,pivot,minY,yTopLimit,rightSideMax) :
    ## 
    if len(plist) < 1 :
        return rightSideMax;

    last_pt = pivot
    
    # Value of the area represented by the histogram from the current "point" in the loop and his predecessor 
    current_skyBottomArea = 0
    # Maximum area represented by an histogram among the list elements
    max_skyBottomArea = 0

    # Highest Y value to create correct rectangles area, initialized to the Y of the first element that will be processed by the loop
    max_y = plist[0][1]
    # Temporary Maximum Area value obtained with a "Y-reduction"
    # Meaning that a new localMax will be calculated each time the current element Y value is lower than the predecessor
    # Calcul uses the formula (currentElementX - pivotX) * max_Y
    localMax = 0

    ##RightSideRun meaning that the list will be processed head to tail going from pivot+1 to last element.   
    for right in range(len(plist)) :
        # Current element "histogram area" is calculated, expressed : ([currentElement-1].X) - currentElement.X * h (height of the global area)
        current_skyBottomArea = (last_pt[0] - plist[right][0]) * h

        # Checking if the current "histogram" area is bigger than the current rightSideMax area
        if current_skyBottomArea > rightSideMax :
            rightSideMax = current_skyBottomArea
        
        
        # /!\ I think I'm missing something here or maybe doing the research of the widest area with "highest Y" the wrong way
        # Note : First time going through is the same result as the first element histogram because yTopLimit[1] is initialized to h (global height)
        if (plist[right][1] < yTopLimit[1]) :
            yTopLimitArea = (plist[right][0] - yTopLimit[0]) * yTopLimit[1]
            #print("==> ----") # DEBUGGING
            #print(plist[right]) # DEBBUGING
            #print(yTopLimitArea) # DEBUGGING
            # Checking if the current "widest area with biggest Y" is bigger than current rightSideMax area
            if yTopLimitArea > rightSideMax :
                rightSideMax = yTopLimitArea
            # Decreasing the yTopLimit Y value to the current element "Y" that becomes the new top limit that we can get in Y. 
            yTopLimit = [yTopLimit[0],plist[right][1]]

        ##Si left.y < max.y 
        # Note : Will always enter here during the first occurence of the loop because max_y is set to the Y of the element that will be treated in first position.
        if (plist[right][1] <= max_y) :
            """ DEBUGGING COMMENTARIES
            print(plist[right])
            print("plus petit ou egal que")
            print(max_y)
            """
            localMax = (plist[right][0] - pivot[0]) * max_y
            #Updating "max_y" value
            max_y = plist[right][1] 

        # Entering else statement means that current point Y is higher than the previous maximal Y obtained.
        # First idea 
        else :
            # Check if the localMax calculated is larger than our actual rightSideMax(Speaking of rightSideMax due to original pivot)
            rightSideMax = max([localMax,rightSideMax,max_skyBottomArea,yTopLimitArea])

            # Here we are looking to save the min Y among all the rightside to create the widest area possible in the righttside
            # Note : Could be useless since the addition of the yTopLimit checking because we should normally get the minY in yTopLimit variable after going through all the list.
            if minY > last_pt[1] :
                minY = last_pt[1]
            return RightSideRun(w,h,plist[plist.index(last_pt)+1:],last_pt,minY,yTopLimit,rightSideMax) #
        
        last_pt = plist[right]
    #print(max_skyBottomArea)
    return max([localMax,max_skyBottomArea,rightSideMax,yTopLimitArea,w*yTopLimit[1]])

def divideToConquer(w,h,n,plist) :
     # First step is to find the point with Y minimal among all points in the original list. That point will become our "pivot"
    mini = min(plist, key=lambda p: p[1])
    # Right after finding the min Y we can add both "extremes points" to the list
    if [0, 0] not in plist :
        plist = [[0, 0]] + plist

    if [w, 0] not in plist :
        plist += [[w, 0]]
        

    #print(mini) # DEBUG 

    # We can already calculate some of the rectangle areas
    # default_max_area will be the widest area possible in the global rectangle, got by miniY * w (width of global rectangle)
    default_max_area = w * mini[1]
    leftside_max = mini[0] * mini[1] 
    rightside_max = (w - mini[0])* (mini[1])  
    minY = h
    yTopLimit = mini#(x)

    # Cutting the orignial list in 2 parts.
    first_half = plist[:plist.index(mini)]
    snd_half = plist[plist.index(mini)+1:]
    
    #print(first_half) # DEBUG
    #print(snd_half) # DEBUG
    #print(default_max_area) # DEBUG

    # Run both sub-functions that will find the largest area in their sublists.(no parallelism)
    leftside_max = LeftSideRun(w,h,first_half,mini,minY,leftside_max)
    rightside_max = RightSideRun(w,h,snd_half,mini,minY,yTopLimit,rightside_max)
    # Getting the true max rectangle area to possibly create
    default_max_area = max([leftside_max,rightside_max,default_max_area])
    print(default_max_area)


def main() :
    lines, points_list = [], []
    l, h, n = 0, 0, 0
    with open('TestsTP1/ex_N10_res24400144 copie') as f:
        lines = f.readlines()
        l = int(lines[0].split()[0])
        h = int(lines[0].split()[1])
        n = int (lines[1])
        for index in range(2, 2 + n) :
            points_list.append([int(lines[index].split()[0]), int(lines[index].split()[1])])
    divideToConquer(l,h,n,points_list)
main()