import time
import matplotlib.pyplot as plt

data_files_list = ["ex_N0_res10000 copie", "ex_N2_res10000 copie", "exCodeChef_N5_res49997500 copie", "ex_N10_res24400144 copie", "ex_N100_res6741 copie", "N200", "N300", "N400", "ex_N500_res7616 copie", "N600", "N700", "N800", "N1000", "N1500", "N2000", "N5000"]


def algo_n_square(l, h, n, liste_de_point) :
    surface_max = 0

    if n == 0 :
        return l * h

    # cas où tous les points sont sur les bords du grand rectangle :
    count = 0
    for point in liste_de_point :
        if point[0] == 0 or point[0] == l or point[1] == 0 or point[1] == h :
            count += 1
    if count == n :
        return l * h
            

    if [0, 0] not in liste_de_point :
        liste_de_point = [[0, 0]] + liste_de_point

    if [l, 0] not in liste_de_point :
        liste_de_point = liste_de_point + [[l, 0]]

    #cas où le point est sur la bordure droite
    for point in range(len(liste_de_point) - 1, 0, -1) :

        if liste_de_point[point][1] == 0 or liste_de_point[point][1] == h or liste_de_point[point][0] == 0 or liste_de_point[point][0] == l or liste_de_point[point - 1][0] == liste_de_point[point][0]:
            if point != len(liste_de_point) - 1 :
                continue

        surface_observee = (liste_de_point[point][0] - liste_de_point[point - 1][0]) * h
        if surface_observee > surface_max :
            surface_max = surface_observee

    #cas où le point est sur la bordure du haut
    for point in range(1, len(liste_de_point) - 1) :
        
        if liste_de_point[point][1] == 0 or liste_de_point[point][1] == h or liste_de_point[point][0] == 0 or liste_de_point[point][0] == l :
            continue
        if liste_de_point[point][0] == liste_de_point[point + 1][0] and liste_de_point[point][1] >= liste_de_point[point + 1][1] :
            continue

        point_gauche, point_droite = 0, 0
        for point_a_sa_droite in range(point + 1, len(liste_de_point)) :
            if liste_de_point[point_a_sa_droite][1] == 0 or liste_de_point[point_a_sa_droite][1] >= liste_de_point[point][1] :
                if point_a_sa_droite != len(liste_de_point) - 1 :
                    continue
            point_droite = point_a_sa_droite
            break

        for point_a_sa_gauche in range(len(liste_de_point[:point]) - 1, -1, -1) :
            if liste_de_point[point_a_sa_gauche][1] == 0 or liste_de_point[point_a_sa_gauche][1] >= liste_de_point[point][1] :
                if point_a_sa_gauche != 0:
                    continue
            point_gauche = point_a_sa_gauche
            break

        surface_observee = (liste_de_point[point_droite][0] - liste_de_point[point_gauche][0]) * liste_de_point[point][1]
        if surface_observee > surface_max :
            surface_max = surface_observee

    return surface_max


liste_de_n, liste_de_temps = [], []

for x in data_files_list :

    lines, liste_de_point = [], []
    l, h, n = 0, 0, 0

    with open("TestsTP1/" + x) as f:
        lines = f.readlines()
        l = int(lines[0].split()[0])
        h = int(lines[0].split()[1])
        n = int (lines[1])
        for index in range(2, 2 + n) :
            liste_de_point.append([int(lines[index].split()[0]), int(lines[index].split()[1])])

    for y in range(2) :
        st = time.process_time()
        result = algo_n_square(l, h, n, liste_de_point)
        et = time.process_time()

        temps_cpu = et - st
        if y == 1 :
            liste_de_temps += [temps_cpu]
            liste_de_n += [n]

        print("Largest rectangle area in ", x, " : \t\t\t\t", result, " m\u00b2")
        print("CPU Time : \t\t\t\t\t\t\t\t\t", temps_cpu, " seconds")
    print()
    print()

print(liste_de_n)
plt.plot(liste_de_n, liste_de_temps)
plt.title("Complexité temporelle de l'algorithme en O(n\u00b2)")
plt.xlim((0, 2500))
plt.ylim((0,0.02))
plt.xlabel('n in point')
plt.ylabel('time in seconds')
plt.show()