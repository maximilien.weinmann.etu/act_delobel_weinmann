Jeremy Delobel && Maximilien Weinmann

lien gitlab : https://gitlab.univ-lille.fr/maximilien.weinmann.etu/act_delobel_weinmann

Pour lancer l'algorithme en n³, une fois dans le dossier TP1, faites : python3 algo_n³.py

pour lancer l'algorithme en n², une fois dans le dossier TP1, faites : python3 algo_n².py

Le fichier **div_to_conquer.py** contient notre essai d'implémentation pour l'algorithme "Div & Conquer" et en temps linéaire.

Le fichier **ebauche1.py** contient notre première ébauche concernant l'implémentation d'un algorithme en n^3 assez différent des algorithmes n^2 et n^3 mis en pratiques.

Dans les deux cas, vous aurez un affichage des valeurs trouvées dans le terminal (surface et temps) ainsi qu'un affichage graphique,
généré avec Matplotlib.


## <span style="color:#bb33aa"> Q1)</span>
### Une première approche peut être basée sur la remarque suivante : un rectangle de surface maximale respectant les contraintes a nécessairement deux sommets de la forme (xi , 0), (xj , 0) avec 0 ≤ i < j ≤ n − 1 (Pourquoi ?).

<br>
C'est parce que la consigne impose que le rectangle ait pour base l'axe des abscisses.
<br><br>



### Comment exprimer la surface du rectangle de surface maximale respectant les contraintes et dont deux sommets sont (xi , 0), (xj , 0) ? Pouvez-vous en déduire un algorithme en Θ(n3 ) ? en Θ(n2 ) ?


Nos algorithmes en n³ et en n² se déroulent en deux étapes :
- Recherche des rectangles de hauteur h
- Recherche des rectangles ayant un point (ou plus) sur le coté horizontal supérieur du rectangle

<br><br>
## <span style="color:#1188ff">Pour l'algorithme en Θ($n^3$) </span>:

<br>
Pour la première étape :
Il s'agit des lignes 14 à 31 de algo_n³.py:

``````python
for point in liste_de_point :
        for autre_point in liste_de_point :
            surface_longueur = abs(point[0] - autre_point[0])

            surface_observee = surface_longueur * h
            temoin = True
            if point[0] < autre_point[0] :
                for point_dans_rectangle in liste_de_point :
                    if point_dans_rectangle[0] > point[0] and point_dans_rectangle[0] < autre_point[0] :
                        temoin = False
                        break
            if point[0] > autre_point[0] :
                for point_dans_rectangle in liste_de_point :
                    if point_dans_rectangle[0] > autre_point[0] and point_dans_rectangle[0] < point[0] :
                        temoin = False
                        break
            if temoin and (surface_observee > surface_max) :
                surface_max = surface_observee
``````

Pour chaque point de notre grand rectangle originel, on dit qu'il est sur un coté vertical d'un possible plus grand rectangle sans point, et on reboucle pour chaque autre_point de notre grand rectangle originel, disant qu'il sera sur l'autre coté du possible plus grand rectangle sans point, c'est ce que gèrent les deux premiers if de l'extrait ci-dessus.
<br><br>
Ensuite au sein de cette boucle imbriqué, on boucle une troisième fois sur chaque point, pour s'assurer qu'ils ne sont pas situés entre les points des deux premières boucles. Si aucun point n'y est situé, alors on a trouvé un possible plus grand rectangle sans point, s'il est le plus grand qu'on a trouvé, on met à jour une variable indiquant le plus grand rectangle trouvé.
<br><br>
Puisqu'il s'agit d'une boucle dans une boucle dans une boucle, toutes bouclants sur les n valeurs, il s'agit d'une complexité en n³.

<br>
<br>
<br>

La deuxième étape est en n² et est la même que dans notre algo en n²(qu'on a trouvé en premier), alors nous l'expliquerons quand nous traiterons de l'algo en n². Nous pouvons déja vous dire que T(n) = n³ + n² autrement dit la compléxité asymptotique de cette algorithme est en Θ($n^3$)

<br><br>

### Observations empiriques :
<br>
N = 5 &nbsp;&nbsp;&nbsp;&nbsp;------->&nbsp;&nbsp;&nbsp;&nbsp;1.5929999999997335e-05  seconds


N = 10&nbsp;&nbsp;&nbsp;&nbsp;------->&nbsp;&nbsp;&nbsp;&nbsp;6.779099999976168e-05  seconds

N = 100&nbsp;&nbsp;&nbsp;&nbsp;------->&nbsp;&nbsp;&nbsp;&nbsp;0.018465899999999813  seconds

N = 500&nbsp;&nbsp;&nbsp;&nbsp;------->&nbsp;&nbsp;&nbsp;&nbsp;1.710630633000001  seconds


#### Graphique Matplotlib :
<img alt="O(n³).png" src="O(n³).png" width="460" height="460" />

<br><br>
## <span style="color:#1188ff">Pour l'algorithme "en Θ($n^2$)" </span>:

En réalité, vous verrez que notre algorithme fait mieux que du n².

<br>
Pour la première étape :
Il s'agit des lignes 29 à 37 de algo_n².py:

``````python
for point in range(len(liste_de_point) - 1, 0, -1) :

        if liste_de_point[point][1] == 0 or liste_de_point[point][1] == h or liste_de_point[point][0] == 0 or liste_de_point[point][0] == l or liste_de_point[point - 1][0] == liste_de_point[point][0]:
            if point != len(liste_de_point) - 1 :
                continue

        surface_observee = (liste_de_point[point][0] - liste_de_point[point - 1][0]) * h
        if surface_observee > surface_max :
            surface_max = surface_observee
``````

Pour chaque point (quasiment, les points extremes ajoutés artificiellement sont parfois omis de nos boucles) de notre grand rectangle originel, en partant du point le plus à droite, on dit qu'il est sur la bordure droite d'un possible plus grand rectangle interne, et on prend son voisin de gauche, et on dit qu'il est sur la bordure de gauche de ce même rectangle interne, on multiplie la distance entre les deux points par la hauteur du grand rectangle originel, et on recommence pour chaque point, c'est très efficace, c'est linéaire, mais on ne trouve pas tous les rectangles possibles, alors il faut passer à la deuxième étape.
<br><br>
Pour la deuxième étape (lignes 40 à 64):

``````python
for point in range(1, len(liste_de_point) - 1) :
        
        if liste_de_point[point][1] == 0 or liste_de_point[point][1] == h or liste_de_point[point][0] == 0 or liste_de_point[point][0] == l :
            continue
        if liste_de_point[point][0] == liste_de_point[point + 1][0] and liste_de_point[point][1] >= liste_de_point[point + 1][1] :
            continue

        point_gauche, point_droite = 0, 0
        for point_a_sa_droite in range(point + 1, len(liste_de_point)) :
            if liste_de_point[point_a_sa_droite][1] == 0 or liste_de_point[point_a_sa_droite][1] >= liste_de_point[point][1] :
                if point_a_sa_droite != len(liste_de_point) - 1 :
                    continue
            point_droite = point_a_sa_droite
            break

        for point_a_sa_gauche in range(len(liste_de_point[:point]) - 1, -1, -1) :
            if liste_de_point[point_a_sa_gauche][1] == 0 or liste_de_point[point_a_sa_gauche][1] >= liste_de_point[point][1] :
                if point_a_sa_gauche != 0:
                    continue
            point_gauche = point_a_sa_gauche
            break

        surface_observee = (liste_de_point[point_droite][0] - liste_de_point[point_gauche][0]) * liste_de_point[point][1]
        if surface_observee > surface_max :
            surface_max = surface_observee
``````
<br><br>
On parcourt chaque point, et on le considère sur la bordure horizontale supérieure d'un possible plus grand rectangle, ce qui signifie qu'un de ses voisins de gauche est sur le bord gauche du rectangle, et qu'un de ses voisins de droite et sur la bordure droite du rectangle, on fera un parcours pour ces deux sens, on a donc deux boucles de même niveau dans une boucle, evidement chacune de ces sous boucles n'a pas à parcourir les n points, puisqu'elle commence à partir d'un point déja au sein du grand rectangle originel, et le voisin juste à coté peut très bien etre le bon, c'est donc une complexité en n * n, soit n² dans les pires des cas (cas où tous les points sont alignés, ou alors s'ils forment un "V"), soit O($n^2$).

Mais dans le cas général et le meilleur des cas, on a une complexité en n log (n), car on a une chance sur deux qu'un voisin directe soit trop haut, une chance sur 4 que le deuxième voisin soit trop haut etc

On peut dire que notre algorithme est en O($n^2$) et en Ω(nlog(n))

<br><br>

### Observations empiriques :
<br>
N = 5 &nbsp;&nbsp;&nbsp;&nbsp;------->&nbsp;&nbsp;&nbsp;&nbsp;1.7201999999993944e-05  seconds


N = 10&nbsp;&nbsp;&nbsp;&nbsp;------->&nbsp;&nbsp;&nbsp;&nbsp;3.5226000000054825e-05  seconds

N = 100&nbsp;&nbsp;&nbsp;&nbsp;------->&nbsp;&nbsp;&nbsp;&nbsp;0.0003376230000000646  seconds

N = 500&nbsp;&nbsp;&nbsp;&nbsp;------->&nbsp;&nbsp;&nbsp;&nbsp;0.002224071000000105  seconds

N = 1000&nbsp;&nbsp;&nbsp;&nbsp;------->&nbsp;&nbsp;&nbsp;&nbsp;0.006317109000000265  seconds

N = 2000&nbsp;&nbsp;&nbsp;&nbsp;------->&nbsp;&nbsp;&nbsp;&nbsp;0.017186646999999944  seconds

N = 5 000&nbsp;&nbsp;&nbsp;&nbsp;------->&nbsp;&nbsp;&nbsp;&nbsp;0.08423356800000015 seconds

N = 100 000&nbsp;&nbsp;&nbsp;&nbsp;------->&nbsp;&nbsp;&nbsp;&nbsp;38 seconds

On peut constater que le temps n'augmente pas en n carré, quand N passe de 100 à 100 000, le temps n'est pas multiplié par 1 million mais par 100 000,de 100 à 1000, au lieu que le temps soit multiplié par 100, il est multiplié par 20.
#### Graphique Matplotlib :
<img alt="O(n²).png" src="O(n²).png" width="460" height="460" />

Sur les graphiques on pourrait presque croire observer un comportement linéaire avec de petits N, mais en prenant des nombres plus grands de N, le nlog n est plus clair.
<br><br>


--- 

# Divide to Conquer

Pour aborder correctement le développement de l'algorithme utilisant le principe "Divide to conquer",
Il est dans un premier temps important de trouver l'élément qui va nous permettre de **diviser** notre liste en 2 parties.
Dans notre cas il s'agit de trouver le point dans la liste possédant la plus petite valeur en ordonnée. C'est-à-dire min(y) dans l'ensemble du tableau.

En partant du principe que min(y) soit unique dans la liste on peut affirmer :
Le point ayant la plus petite ordonnée sera dans tous les cas l'abscisse de début ou de fin du plus grand rectangle réalisable.

En réalité, il y a une exception à cela, dans le cas ou le plus grand rectangle réalisable à pour longueur "w", 
on calculera alors la surface du plus grand rectangle selon la formule suivante : width - 0 * min(y) 

Une fois le **pivot** trouvé on peut alors diviser et traiter notre liste en 2 sous-listes distinctes.

On établira que la surface maximale par défaut du côté gauche du **pivot** sera calculée par ((pivot.x - 0) * pivot.y)
De la même manière, la surface maximale par défaut de la partie du tableau à droite du **pivot** sera calculée par (width - pivot.x) * pivot.y)

<img src="Annexe_DToC_1.PNG" alt="Annexe_DToC_1" width="500"/>

## Déroulement de l'algorithme 

Nous n'avons pas réussi à "implémenter" un algorithme fonctionnel qui respecte le principe "Divide to Conquer" cependant nous avons explorer plusieurs pistes permettant d'atteindre le résultat attendu.

## Pistes "concrètes" 

L'une de nos pistes la plus prometteuse consistait à :
```
- Chercher le point de division CàD le Y minimum dans la liste
- Executer les deux sous-fonctions récursives de part et d'autre de la liste
- En déduire la surface rectangulaire maximale
```

On choisit de couper la liste en deux selon le Y minimal de celle-ci afin d'en extraire le premier **pivot**.

Suite à cela nous pouvons exécuter les deux fonctions "semi-recursives" (en réalité pas de récursivité) **LeftSideRun()** et **RightSideRun()** afin de trouver la surface rectangulaire maximale au sein de deux sublists crées par la découpe.

La différence entre ses deux fonctions est minime, il s'agit surtout de l'ordre dans lequel les points de la "sublist" seront traités.
Pour **LeftSideRun** on traitera les points allant de **[0;indice_du_pivot_-1]** de l'indice le plus gros à celui le plus petit. **décroissante**
A l'inverse dans **RightSideRun** les points allant de **[indice_du_pivot_+1;n]** de la liste originale seront traités avec de façon **croissante**. 

La fonction consiste alors à parcourir les points de la sublist en essayant à chaque fois de créer tous les rectangles possibles à partir du point courant et son prédécesseur.
Un appel à la même fonction est réalisé lorsque l'on trouve un point d'ordonnée supérieur au précédent. Dans ce cas on réappelle la fonction sur le restant de la sous-liste à traiter en prenant comme nouveau pivot le point de *"plus petite"* ordonné dans la partie déjà exploré.
Une fois ces traitement terminé, il suffirait (en théorie) d'éxécuter l'instruction ci-dessous pour obtenir la réponse au problème donnée.
```py
print(max([leftSideRun(...),RightSideRun(...),default_max_area]))
```


**Nous pensons que cette "solution" est en 0(n) car il n'y a qu'un seul balayage de la liste qui est effectué dans la recherche de la plus grande surface en plus du balayage en O(n) pour trouver le premier pivot permettant de séparer la liste en deux.**

Nous avons essayé d'implémenter cet algorithme dans le fichier **div_to_conquer.py**, après divers tests nous nous aperçevons qu'il manque certains calculs de surfaces rectangulaires à cause de se changement de pivot.

### Limites et/ou difficultés rencontrés :


La principale difficulté rencontré lors de l'implémentation de cette algorithme et aussi celle qui nous fait encore défaut pour ce rendu est **l'oubli de calculer certaines surfaces rectangulaires valides** lors du balayage de la liste.

Certains cas comme le calcul des zones dites "histogrammes" ou encore du rectangle "le plus large" (voir les commentaires dans div_to_conquer.py) ont été oubliées lors de notre réfléxion, ce qui a mené à divers rajouts au fur et à mesure  dans notre algorithme jusqu'à se perdre complétement dans les cas déjà traités et non traités par l'algorithme.


### Pistes non aboutis


#### Récursivité totale et recherche du Y minimale en continu

Une autre de nos idées (non approfondie) pour aborder ce problème en utilisant la méthode "Divide to Conquer" était de se dire de toujours chercher 
le Y minimum dans chaque sous partie de tableau jusqu'à obtenir uniquement des sous tableaux vides.
Modélisation sous forme d'arbre.
 
Questions non résolues ?
=> Remonter les branches en calculant à chaque fois les surfaces réalisables entre une feuille et son noeud. Bonne démarche ??
=> Comment gérer la recomposition ?
=> Quels points délimitant choisir pour les rectangles ?
=> Pouvons nous rapprocher cette modélisation du problème à une forme d'arbre connue ?

# Algorithme en temps linéaire.

## Temps linéaire ou "Divide to Conquer" ???

Nous pensons que nous nous sommes égarés dans la réalisation de l'algorithme "Divide To Conquer".
En effet, il semblerait que nous soyons "mi-figue mi-raisin" entre l'algorithme en temps linéaire 0(n) et celui "Divide To conquer" attendu.

### L'implémentation

Vous pourrez retrouver notre tentative d'implémentation de l'algorithme en temps linéaire dans le fichier *div_to_conquer.py*.
N'étant pas fiables concernant la résolution du problème, nous n'avons pas pu assurer un bon calcul de la complexité de cet algorithme.
Nous supposons qu'il s'agit d'un algorithme en 0(n) car seulement deux balayages de la liste (complète) est effectués :(Lors de l'utilisation de la fonction min() sur la liste complète) pour trouver le pivot. Ainsi que le balayage de chaque sublist par les fonctions **LeftSideRun** et **RightSideRun**
