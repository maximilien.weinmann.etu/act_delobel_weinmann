import time
import matplotlib.pyplot as plt

data_files_list = ["ex_N0_res10000 copie", "ex_N2_res10000 copie", "exCodeChef_N5_res49997500 copie", "ex_N10_res24400144 copie", "ex_N100_res5980 copie", "ex_N100_res6741 copie", "ex_N500_res7616 copie", "ex_N500_res7854 copie", "ex_N100000_res100000 copie"]


def algo_n(l, h, n, liste_de_point) :
    surface_max = 0

    return surface_max


liste_de_n, liste_de_temps = [], []

for x in data_files_list :

    lines, liste_de_point = [], []
    l, h, n = 0, 0, 0

    with open("TestsTP1/" + x) as f:
        lines = f.readlines()
        l = int(lines[0].split()[0])
        h = int(lines[0].split()[1])
        n = int (lines[1])
        for index in range(2, 2 + n) :
            liste_de_point.append([int(lines[index].split()[0]), int(lines[index].split()[1])])

    for y in range(2) :
        st = time.process_time()
        result = algo_n(l, h, n, liste_de_point)
        et = time.process_time()

        temps_cpu = et - st
        if y == 1 :
            liste_de_temps += [temps_cpu]
            liste_de_n += [n]

        print("Largest rectangle area in ", x, " : \t\t\t\t", result, " m\u00b2")
        print("CPU Time : \t\t\t\t\t\t\t\t\t", temps_cpu, " seconds")
    print()
    print()

print(liste_de_n)
plt.plot(liste_de_n, liste_de_temps)
plt.title("Complexité temporelle de l'algorithme en O(n)")
plt.xlabel('n in point')
plt.ylabel('time in seconds')
plt.show()