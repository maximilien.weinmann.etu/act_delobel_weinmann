from random import randint

liste = []
for _ in range(5000) :
    value = [[randint(0, 100), randint(0, 500)]]
    liste += value

liste.sort()


with open('N5000', 'w') as f:
    for point in liste:
        px = str(point[0])
        py = str(point[1])
        f.write(px + " " + py)
        f.write('\n')