import time
import matplotlib.pyplot as plt

data_files_list = ["ex_N0_res10000 copie", "ex_N2_res10000 copie", "exCodeChef_N5_res49997500 copie", "ex_N10_res24400144 copie", "ex_N60_res", "ex_N70_res", "ex_N80_res", "ex_N90_res", "ex_N100_res5980 copie", "ex_N100_res6741 copie", "ex_N150_res", "ex_N500_res7616 copie", "ex_N500_res7854 copie"]


def algo_n_cube(l, h, n, liste_de_point) :
    surface_max = 0

    if n == 0 :
        return l * h

    for point in liste_de_point :
        for autre_point in liste_de_point :
            surface_longueur = abs(point[0] - autre_point[0])

            surface_observee = surface_longueur * h
            temoin = True
            if point[0] < autre_point[0] :
                for point_dans_rectangle in liste_de_point :
                    if point_dans_rectangle[0] > point[0] and point_dans_rectangle[0] < autre_point[0] :
                        temoin = False
                        break
            if point[0] > autre_point[0] :
                for point_dans_rectangle in liste_de_point :
                    if point_dans_rectangle[0] > autre_point[0] and point_dans_rectangle[0] < point[0] :
                        temoin = False
                        break
            if temoin and (surface_observee > surface_max) :
                surface_max = surface_observee
    

    for point in range(1, len(liste_de_point) - 1) :
        
        if liste_de_point[point][1] == 0 or liste_de_point[point][1] == h or liste_de_point[point][0] == 0 or liste_de_point[point][0] == l :
            continue
        if liste_de_point[point][0] == liste_de_point[point + 1][0] and liste_de_point[point][1] >= liste_de_point[point + 1][1] :
            continue

        point_gauche, point_droite = 0, 0
        for point_a_sa_droite in range(point + 1, len(liste_de_point)) :
            if liste_de_point[point_a_sa_droite][1] == 0 or liste_de_point[point_a_sa_droite][1] >= liste_de_point[point][1] :
                if point_a_sa_droite != len(liste_de_point) - 1 :
                    continue
            point_droite = point_a_sa_droite
            break

        for point_a_sa_gauche in range(len(liste_de_point[:point]) - 1, -1, -1) :
            if liste_de_point[point_a_sa_gauche][1] == 0 or liste_de_point[point_a_sa_gauche][1] >= liste_de_point[point][1] :
                if point_a_sa_gauche != 0:
                    continue
            point_gauche = point_a_sa_gauche
            break

        surface_observee = (liste_de_point[point_droite][0] - liste_de_point[point_gauche][0]) * liste_de_point[point][1]
        if surface_observee > surface_max :
            surface_max = surface_observee

    return surface_max


liste_de_n, liste_de_temps = [], []

for x in data_files_list :

    lines, liste_de_point = [], []
    l, h, n = 0, 0, 0

    with open("TestsTP1/" + x) as f:
        lines = f.readlines()
        l = int(lines[0].split()[0])
        h = int(lines[0].split()[1])
        n = int (lines[1])
        for index in range(2, 2 + n) :
            liste_de_point.append([int(lines[index].split()[0]), int(lines[index].split()[1])])

    for y in range(2) :
        st = time.process_time()
        result = algo_n_cube(l, h, n, liste_de_point)
        et = time.process_time()

        temps_cpu = et - st
        if y == 1 :
            liste_de_temps += [temps_cpu]
            liste_de_n += [n]

        print("Largest rectangle area in ", x, " : \t\t\t\t", result, " m\u00b2")
        print("CPU Time : \t\t\t\t\t\t\t\t\t", temps_cpu, " seconds")
    print()
    print()

print(liste_de_n)
plt.plot(liste_de_n, liste_de_temps)
plt.title("Complexité temporelle de l'algorithme en O(n\u00b3)")
plt.ylim((0,0.1))
plt.xlabel('n in point')
plt.ylabel('time in seconds')
plt.show()