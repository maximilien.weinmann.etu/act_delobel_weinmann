# Ce fichier est notre première approche et première tentative d'algorithme pour essayer de résoudre
# la première question posée dans le TP, il s'agissait ici d'une approche en n^3

#Q1 Une première approche

def algo_n_cube(l,h, points_list) :
    points_list.sort()
    surface_max = 0

    if [0, 0] not in points_list :
        points_list = [[0, 0]] + points_list

    if [l, 0] not in points_list :
        points_list += [[l, 0]]


    surface_max = 0
    for point_gauche in points_list :
        tmp_surface = 0
        for point_droite in  points_list :
            if point_droite[0] <= point_gauche[0] :
                continue
            grandY = max(point_gauche[1],point_droite[1]) # Plus grande ordonnée entre PG et PD
            for point_milieu in points_list : 
                if ( point_milieu[0] < point_gauche[0]) or (point_milieu[0] > point_droite[0]) : # On check si le pt n'est pas à l'interieur de la zone PG/PD, on l'ignore 
                    continue
                else :
                    
                    grandY = max(grandY,point_milieu[1]) # On prend le max entre l'ordonné la plus haute précedemment obtenu et celle du pt milieu
                    if (point_milieu[1] < grandY) : # Si ordonnée pt milieu STRICTEMENT plus petite bah on a une nouvelle zone rectangulaire
                        grandY = point_milieu[1] # On indique mnt que la plus grande ordonnée devient celle de pt_milieu
                        tmp_surface = (point_droite[0] - point_gauche[0]) * grandY # Calcul de la nouvelle surface
                        if(tmp_surface < surface_max) : # Juste pour éviter quelques tours de boucles inutiles, si la plus grande zone qu'on peut faire ici est déjà plus petite que s_max ca sert a rien de poursuivre
                            break
            #print("----------")
            # Normalement une fois tous les pts du milieu parcourus on se retrouve avec la surface du plus grand rectangle possible dans PG/PD
            if (tmp_surface > surface_max) : # Ici on devrait avoir  le cas du 5980 > 5540 
                print("----------")
                print("JE RENTRE ICI")
                surface_max = tmp_surface
                    
                    
        

    return surface_max
