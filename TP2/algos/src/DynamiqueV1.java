import java.util.ArrayList;
import java.util.Scanner;
import java.util.HashMap;
import java.util.Map;

public class DynamiqueV1 {
    
    private static Map<String, Integer> memo = new HashMap<>();
    
    /**
     * Fonction simple de deep copy d'un int[][]
     * @param currentConfiguration la configuration à copier
     * @return la copie
     */
    public static int[][] deepCopy(int[][] currentConfiguration) {
        int[][] copied = new int[currentConfiguration.length][currentConfiguration[0].length];
        for (int ligne = 0; ligne < currentConfiguration.length; ligne++) {
            copied[ligne] = currentConfiguration[ligne].clone();
        }
        return copied;
    }

    /**
     * Un simple afficheur de Configuration assez propre
     * @param config la configuration à afficher
     */
    public static void printConfig(int config[][]) {
        System.out.println();
        System.out.println("Telle est la config : ");
        System.out.println();
        for (int i = 0; i < config.length; i++) {
            System.out.print("[");
            for (int j = 0; j < config[i].length; j++) {
                if (config[i][j] == -1) {
                    System.out.print("-1 ");
                } else if (config[i][j] == 0) {
                    System.out.print(" 0 ");
                } else if (config[i][j] == 1) {
                    System.out.print("+1 ");
                }
            }
            System.out.println("]");
        }
    }

    /**
     * Permet de savoir si un pion ennemi se retrouve sur la ligne qui fait gagner l'adversaire
     * @param currentPlayer le joueur duquel c'est le tour
     * @param configuration la configuraton du jeu 
     * @param n la longueur verticale de la configuration
     * @param m la longueur horizontale de la configuration
     * @return vrai ou faux en fonction de sa position
     */
    public static boolean isEnemyLastLine (int currentPlayer, int[][] configuration, int n, int m) {
        if (currentPlayer == 1) {
            for (int i = 0; i < m; i++) {
                if (configuration[n - 1][i] == 0) {
                    return true;
                }
            }
        } else {
            for (int i = 0; i < m; i++) {
                if (configuration[0][i] == 1) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Détermine une configuration enfant de currentConfiguration
     * @param currentConfiguration la configuraton du jeu avant permutation
     * @param position la position d'un pion avant son mouvement
     * @param destination la position d'un pion après son mouvement
     * @return Returns a configuration in which a pawn moved given its position, destination and original board
     */
    public static int[][] permute(int[][] currentConfiguration, int[] position, int[] destination) {
        int[][] newConfig = deepCopy(currentConfiguration);
        int tmp = currentConfiguration[position[0]][position[1]];
        newConfig[position[0]][position[1]] = -1;
        newConfig[destination[0]][destination[1]] = tmp;

        return newConfig;
    }

    /**
     * nextConfigs trouve toutes les configurations enfants possible à partir d'une configuration mère
     * @param currentPlayer le joueur duquel c'est le tour
     * @param configuration la configuraton du jeu 
     * @param n la longueur verticale de la configuration
     * @param m la longueur horizontale de la configuration
     * @return les configurations possibles
     */
    public static ArrayList<int[][]> nextConfigs(int currentPlayer, int[][] configuration, int n, int m, ArrayList<String> successorsKeys) {
        ArrayList<int[][]> configs = new ArrayList<int[][]>();
        if (currentPlayer == 1) {
            for (int x = 0; x < n; x++) {
                for (int y = 0; y < m; y++) {
                    if (configuration[x][y] == 1) {
                        if (x - 1 > -1) {   // Checking borders of the board
                            if (configuration[x - 1][y] == -1) {        // Going forward (Free space in front of the pawn)
                                int[] position = new int[]{x, y};
                                int[] destination = new int[]{x - 1, y};
                                int[][] newConfig = permute(configuration, position, destination);
                                successorsKeys.add(newConfig.toString());
                                configs.add(newConfig);    // Appending to all possible configs, the configuration where the current pawn moved forward
                            } if (y - 1 > -1) {     // Checking borders
                                if (configuration[x - 1][y - 1] == 0) {     // Testing if we can move by killing the ennemy pawn in diagonal left
                                    int[] position = new int[]{x, y};
                                    int[] destination = new int[]{x - 1, y - 1};
                                    int[][] newConfig = permute(configuration, position, destination);
                                    successorsKeys.add(newConfig.toString());
                                    configs.add(newConfig);    // Appending to all possible configs, the configuration where the current pawn moved forward     // Appending to all possible configs, the configuration where the current pawn moved in diagonal left by eating the ennemy pawn
                                }
                            } if (y + 1 < m) {     // Checking borders
                                if (configuration[x - 1][y + 1] == 0) {     // Testing if we can move by killing the ennemy pawn in diagonal right
                                    int[] position = new int[]{x, y};
                                    int[] destination = new int[]{x - 1, y + 1};
                                    int[][] newConfig = permute(configuration, position, destination);
                                    successorsKeys.add(newConfig.toString());
                                    configs.add(newConfig);    // Appending to all possible configs, the configuration where the current pawn moved forward     // Appending to all possible configs, the configuration where the current pawn moved in diagonal right by eating the ennemy pawn
                                }
                            }
                        }
                    }
                }
            }
        } else if (currentPlayer == 0) {
            for (int x = 0; x < n; x++) {
                for (int y = 0; y < m; y++) {
                    if (configuration[x][y] == 0) {
                        if (x + 1 < n) {    // Checking borders of the board
                            if (configuration[x + 1][y] == -1) {
                                int[] position = new int[]{x, y};
                                int[] destination = new int[]{x + 1, y};
                                int[][] newConfig = permute(configuration, position, destination);
                                successorsKeys.add(newConfig.toString());
                                configs.add(newConfig);    // Appending to all possible configs, the configuration where the current pawn moved forward
                            } if (y - 1 > -1) {     // Checking borders of the board
                                if (configuration[x + 1][y - 1] == 1) {
                                    int[] position = new int[]{x, y};
                                    int[] destination = new int[]{x + 1, y - 1};
                                    int[][] newConfig = permute(configuration, position, destination);
                                    successorsKeys.add(newConfig.toString());
                                    configs.add(newConfig);    // Appending to all possible configs, the configuration where the current pawn moved forward     // Testing if we can move by killing the ennemy white pawn in diagonal left
                                }
                            } if (y + 1 < m) {
                                if (configuration[x + 1][y + 1] == 1) {
                                    int[] position = new int[]{x, y};
                                    int[] destination = new int[]{x + 1, y + 1};
                                    int[][] newConfig = permute(configuration, position, destination);
                                    successorsKeys.add(newConfig.toString());
                                    configs.add(newConfig);    // Appending to all possible configs, the configuration where the current pawn moved forward     // Testing if we can move by killing the ennemy white pawn in diagonal right
                                }
                            }
                        }
                    }
                }
            }
        }
        return configs;
    }

    /**
     * Un filtre pour retrouver la plus grande valeur négative d'un liste d'entier
     * @param maxValues array des valeurs maximales pour les configurations enfants trouvées
     * @return la plus grande valeur négative d'un liste d'entier
     */
    public static int where(Integer[] maxValues) {
        int greatestNegative = Integer.MIN_VALUE;
        for (int valIndex = 0; valIndex < maxValues.length; valIndex++) {
            if ((maxValues[valIndex] < 0) && maxValues[valIndex] > greatestNegative) {
                greatestNegative = maxValues[valIndex];
            }
        }
        return greatestNegative;
    }

    /**
     * Donne la plus grande valeur d'un array d'entiers positifs
     * @param maxValues L'array pour lequel on cherche le max
     * @return int, La plus grande valeur
     */
    public static int max(Integer[] maxValues) {
        int greatest = 0;
        for (int valIndex = 0; valIndex < maxValues.length; valIndex++) {
            if ( maxValues[valIndex] > greatest) {
                greatest = maxValues[valIndex];
            }
        }
        return greatest;
    }

    /**
     * Trouve les configurations enfants par récursion jusqu'à en trouver une gagnante, puis affiche le nombre de coup avant la victoire/défaite
     * garde les configurations trouvées en mémoire
     * @param n la longueur verticale de la configuration
     * @param m la longueur horizontale de la configuration
     * @param configuration la configuration à partir de laquelle évaluer la situation
     * @param currentPlayer le joueur duquel c'est le tour
     * @return Le nombre de coup avant la victoire/défaite
     */
    public static int mainFct(int n, int m, int[][] configuration, int currentPlayer) {
        /* Memoization 
        *
        */
        String configKey = configuration.toString();
        if(memo.containsKey(configKey)) {
            return memo.get(configKey);
        }
        if (isEnemyLastLine(currentPlayer, configuration, n, m)) {
            return 0;
        }
        /*
         * Memoization 
         */
        ArrayList<String> successorsKeys = new ArrayList<String>();

        ArrayList<int[][]> successors = nextConfigs(currentPlayer, configuration, n, m,successorsKeys);

        if (successors.isEmpty()) {
            return 0;
        }
        Integer[] maxValues = new Integer[successors.size()];
        boolean negativeFlag = false;

        if (currentPlayer == 1) {
            currentPlayer = 0;
        } else {
            currentPlayer = 1;
        }

        for (int configIndex = 0; configIndex < successors.size(); configIndex++) {
            if (memo.containsKey(successorsKeys.get(configIndex))) {
                maxValues[configIndex] = memo.get(successorsKeys.get(configIndex));
            } else {
                maxValues[configIndex] = mainFct(n, m, successors.get(configIndex), currentPlayer);
            }


            if (maxValues[configIndex] == 0) {
                return 1;
            }
            if (maxValues[configIndex] < 0) {
                negativeFlag = true;
            }
        }

        if (negativeFlag) {
            int value = (-1 * where(maxValues)) + 1;
            memo.put(configKey,value);
            return value;
        }
        int greatestPositive = max(maxValues);
        int value = (-1 * greatestPositive) - 1;
        memo.put(configKey,value);
        return value;
    }

    public static void main(String[] args) throws Exception {
        Scanner myScan = new Scanner(System.in);
        
        System.out.println("Enter n (vertical): ");
        int n = Integer.parseInt(myScan.nextLine());
        System.out.println("Enter m (horizontal): ");
        int m = Integer.parseInt(myScan.nextLine());
        int[][] inputConfig = new int[n][m];

        System.out.println("Enter configuration : ");
        // ------------------------------------------------------------LECTURE DE LA DATA
        for (int x = 0; x < n; x++) {
            char[] values = myScan.nextLine().toCharArray();
            for (int y = 0; y < m; y++) {
                if (values[y] == 'P') {
                    inputConfig[x][y] = 1;
                } else if (values[y] == 'p') {
                    inputConfig[x][y] = 0;
                } else {
                    inputConfig[x][y] = -1;
                }
            }
        }
        
        System.out.println(mainFct(n, m, inputConfig, 1));
        
        myScan.close();
    }
}