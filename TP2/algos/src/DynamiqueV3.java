import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Tried here an implementation of the Zobrist hashing method to avoid colliding problems when hashing in order to memoize
 * Inspired by https://levelup.gitconnected.com/zobrist-hashing-305c6c3c54d0 for the method
 * @see {@link}https://en.wikipedia.org/wiki/Zobrist_hashing
 */
final class ZobristHexapawn {
    public int hash; // Current hashcode
    private int [][] keys; // Array in which we save keys of every available board cell
    static final int NB_POSSIBILITIES = 2; // Black and White
    /**
     * Generates random keys for every available board cell and every possible game.
     * Sets the hash to 0, (prepared to compute values).
     * @param n row of the board game
     * @param m column of the board game
     */
    public ZobristHexapawn(int n, int m) {
        int positions = n*m;
        this.keys = new int [NB_POSSIBILITIES][positions];
        for (int i = 0;i < 2;i++) {
            for(int j = 0;j < positions;j++) {
                keys[i][j] = (int) (((long) (Math.random() * Long.MAX_VALUE)) & 0xFFFFFFFF);
            }
        }
        this.hash = 0;
    }
    /**
     * Allows to reset the hash attribute in order to calculate hashcode for a new configuration
     */
    public void resetHash() {
        this.hash = 0;
    }
    /**
     * Allows to easily find the hashcode of the new configuration with the ^(XOR) operator
     * @param piece type of the pawn, 0 -> black pawn ; 1 -> white pawn 
     * @param position position in the board expressed under the formula row * column
     * @return the computed hash
     */
    public int computeHash(int piece,int position) {
        this.hash =  this.hash ^ keys[piece][position];
        return this.hash;
    }
    /**
     * Calculate the hashcode of current configuration after removing the piece pawn at the given position
     * @param piece type of the pawn, 0 -> black pawn ; 1 -> white pawn 
     * @param position position in the board expressed under the formula row * column
     * @return the new hashcode value
     */
    public int remove(int piece, int position) {
        return computeHash(piece, position);
    }
    /**
     * Calculate the hashcode of current configuration after adding the piece pawn at the given position
     * @param piece type of the pawn, 0 -> black pawn ; 1 -> white pawn 
     * @param position position in the board expressed under the formula row * column
     * @return the new hashcode value
     */
    public int add(int piece, int position) {
        return computeHash(piece, position);
    }
    /**
     * Simple getter of hash value
     */
    public int getHash() {
        return this.hash;
    }
    /**
     * Simple setter of hash value
     * We set the hash value back to the current configuration after exploring the next_configs function
     */
    public void setHash(int hash) {
        this.hash = hash;
    }

}
/**
 * Our third attempt of implementing a memoized algorithm for Hexapawn that computes all the test of {@link}https://contest.fil.univ-lille.fr/contest/classic/show/tp2-master1-hexapawn
 * This time we are using a Zobrist Hashing approach to keep configurations IDs
 */
public class DynamiqueV3 {
    private static Map<Integer, Integer> memo = new HashMap<>();

    /**
     * Probably the most annoying function of our algorithm because we can't avoid being in O(N^2) to deepcopy a 2D Array
     * @param currentConfiguration the current configuration to be copied
     * @return the copied configuration
     */
    public static int[][] deepCopy(int[][] currentConfiguration) {
        int[][] copied = new int[currentConfiguration.length][currentConfiguration[0].length];
        for(int i = 0; i < currentConfiguration.length; i++){
            System.arraycopy(currentConfiguration[i], 0, copied[i], 0, currentConfiguration[0].length);
        }
        return copied;
    }

    /**
    * A display function made and use for debug purpose only.
    * @param config the configuration to display
    */
    public static void printConfig(int config[][]) {
        System.out.println();
        System.out.println("Telle est la config : ");
        System.out.println();
        for (int i = 0; i < config.length; i++) {
            System.out.print("[");
            for (int j = 0; j < config[i].length; j++) {
                if (config[i][j] == -1) {
                    System.out.print("-1 ");
                } else if (config[i][j] == 0) {
                    System.out.print(" 0 ");
                } else if (config[i][j] == 1) {
                    System.out.print("+1 ");
                }
            }
            System.out.println("]");
        }
    }

    /**
     * Check if an ennemy pawn is in the currentPlayer spawn area
     * If "WHITE" is playing check if a BLACK pawn is in last row of the board
     * If "BLACK" is playing check if a WHITE pawn is in first row of the board
     * @param currentPlayer 0 -> Black / 1 -> White
     * @param configuration current configuration
     * @param n amount of row of the board
     * @param m amount of colmun of the board
     * @return {@code}true{@code} if an ennemy has reached the current player spawn,  {@code}false{@code} if not
     */
    public static boolean isEnemyLastLine (int currentPlayer, int[][] configuration, int n, int m) {
        if (currentPlayer == 1) {
            for (int i = 0; i < m; i++) {
                if (configuration[n - 1][i] == 0) {
                    return true;
                }
            }
        } else {
            for (int i = 0; i < m; i++) {
                if (configuration[0][i] == 1) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns a configuration in which a pawn moved given its position, destination and original board
     * @param currentConfiguration current configuration of the board
     * @param position current position of the pawn that has to move
     * @param destination position where the pawn will move
     * @return configuration in which the pawn at position moved to destination
     */
    public static int[][] permute(int[][] currentConfiguration, int[] position, int[] destination) {
        int[][] newConfig = deepCopy(currentConfiguration);
        int tmp = currentConfiguration[position[0]][position[1]];
        newConfig[position[0]][position[1]] = -1;
        newConfig[destination[0]][destination[1]] = tmp;

        return newConfig;
    }
    //TO-DO
    /**
     * Calculate successors of the currentConfig
     * @param currentPlayer 0 -> Black / 1 -> White 
     * @param configuration currentPlayer
     * @param n amount of row of the game board
     * @param m amount of column of the game board
     * @param keys ArrayList<Integer> to save hashCode of successors configurations 
     * @param zobrist Zobrist Hashing keys to easily calculate successors configurations hashCode
     * @return
     */
    public static ArrayList<int[][]> nextConfigs(int currentPlayer, int[][] configuration, int n, int m,ArrayList<Integer> keys,ZobristHexapawn zobrist) {
        int configurationHash = zobrist.getHash();
        ArrayList<int[][]> configs = new ArrayList<int[][]>();
        if (currentPlayer == 1) {
            for (int x = 0; x < n; x++) {
                for (int y = 0; y < m; y++) {
                    if (configuration[x][y] == 1) { // White checking
                        if (x - 1 > -1) {   // Checking borders of the board
                            if (configuration[x - 1][y] == -1) {        // Going forward (Free space in front of the pawn)
                                int[] position = new int[]{x, y}; 
                                int[] destination = new int[]{x - 1, y};
                                int[][] newConfig = permute(configuration, position, destination);
                                configs.add(newConfig);    // Appending to all possible configs, the configuration where the current pawn moved forward
                                // Calculate the hashCode() of the successor config using zobrist hashing properties
                                zobrist.remove(configuration[x][y], x * y);
                                zobrist.add(configuration[x][y], destination[0] * destination[1]);
                                keys.add(zobrist.getHash());
                                zobrist.setHash(configurationHash);
                            } if (y - 1 > -1) {     // Checking borders
                                if (configuration[x - 1][y - 1] == 0) {     // Testing if we can move by killing the ennemy pawn in diagonal left
                                    int[] position = new int[]{x, y};
                                    int[] destination = new int[]{x - 1, y - 1};
                                    int[][] newConfig = permute(configuration, position, destination);
                                    configs.add(newConfig);    // Appending to all possible configs, the configuration where the current pawn moved forward     // Appending to all possible configs, the configuration where the current pawn moved in diagonal left by eating the ennemy pawn
                                    // Calculate the hashCode() of the successor config using zobrist hashing properties
                                    zobrist.remove(configuration[x][y], x * y);
                                    zobrist.remove(configuration[destination[0]][destination[1]], destination[0] * destination[1]);
                                    zobrist.add(configuration[x][y], destination[0] * destination[1]);
                                    keys.add(zobrist.getHash());
                                    zobrist.setHash(configurationHash);
                                }
                            } if (y + 1 < m) {     // Checking borders
                                if (configuration[x - 1][y + 1] == 0) {     // Testing if we can move by killing the ennemy pawn in diagonal right
                                    int[] position = new int[]{x, y};
                                    int[] destination = new int[]{x - 1, y + 1};
                                    int[][] newConfig = permute(configuration, position, destination);
                                    configs.add(newConfig);    // Appending to all possible configs, the configuration where the current pawn moved forward     // Appending to all possible configs, the configuration where the current pawn moved in diagonal right by eating the ennemy pawn
                                    // Calculate the hashCode() of the successor config using zobrist hashing properties
                                    zobrist.remove(configuration[x][y], x * y);
                                    zobrist.remove(configuration[destination[0]][destination[1]], destination[0] * destination[1]);
                                    zobrist.add(configuration[x][y], destination[0] * destination[1]);
                                    keys.add(zobrist.getHash());
                                    zobrist.setHash(configurationHash);
                                }
                            }
                        }
                    }
                }
            }
        } else if (currentPlayer == 0) {
            for (int x = 0; x < n; x++) {
                for (int y = 0; y < m; y++) {
                    if (configuration[x][y] == 0) {
                        if (x + 1 < n) {    // Checking borders of the board
                            if (configuration[x + 1][y] == -1) {
                                int[] position = new int[]{x, y};
                                int[] destination = new int[]{x + 1, y};
                                int[][] newConfig = permute(configuration, position, destination);
                                configs.add(newConfig);    // Appending to all possible configs, the configuration where the current pawn moved forward
                                // Calculate the hashCode() of the successor config using zobrist hashing properties
                                zobrist.remove(configuration[x][y], x * y);
                                zobrist.add(configuration[x][y], destination[0] * destination[1]);
                                keys.add(zobrist.getHash());
                                zobrist.setHash(configurationHash);
                            } if (y - 1 > -1) {     // Checking borders of the board
                                if (configuration[x + 1][y - 1] == 1) {
                                    int[] position = new int[]{x, y};
                                    int[] destination = new int[]{x + 1, y - 1};
                                    int[][] newConfig = permute(configuration, position, destination);
                                    configs.add(newConfig);    // Appending to all possible configs, the configuration where the current pawn moved forward     // Testing if we can move by killing the ennemy white pawn in diagonal left
                                    // Calculate the hashCode() of the successor config using zobrist hashing properties
                                    zobrist.remove(configuration[x][y], x * y);
                                    zobrist.remove(configuration[destination[0]][destination[1]], destination[0] * destination[1]);
                                    zobrist.add(configuration[x][y], destination[0] * destination[1]);
                                    keys.add(zobrist.getHash());
                                    zobrist.setHash(configurationHash);
                                }
                            } if (y + 1 < m) {
                                if (configuration[x + 1][y + 1] == 1) {
                                    int[] position = new int[]{x, y};
                                    int[] destination = new int[]{x + 1, y + 1};
                                    int[][] newConfig = permute(configuration, position, destination);
                                    configs.add(newConfig);    // Appending to all possible configs, the configuration where the current pawn moved forward     // Testing if we can move by killing the ennemy white pawn in diagonal right
                                    // Calculate the hashCode() of the successor config using zobrist hashing properties
                                    zobrist.remove(configuration[x][y], x * y);
                                    zobrist.remove(configuration[destination[0]][destination[1]], destination[0] * destination[1]);
                                    zobrist.add(configuration[x][y], destination[0] * destination[1]);
                                    keys.add(zobrist.getHash());
                                    zobrist.setHash(configurationHash);
                                }
                            }
                        }
                    }
                }
            }
        }
        return configs;
    }
    /**
     * Check the presence of negative values among all successors configurations values.
     * @param maxValues Array of successors values
     * @return the greatestNegative among the array
     */
    public static int where(Integer[] maxValues) {
        Integer[] filtered = new Integer[maxValues.length];
        for (int valueIndex = 0; valueIndex < maxValues.length; valueIndex++) {
            if (maxValues[valueIndex] < 0) {
                filtered[valueIndex] = maxValues[valueIndex];
            } else {
                filtered[valueIndex] = Integer.MIN_VALUE;
            }
        }
        int greatestNegative = Collections.max(Arrays.asList(filtered));
        return greatestNegative;
    }
    /**
    * Recursive function that calculates the value of a configuration (meaning either the smallest amount of rounds before winning OR the greatest amount of rounds before losing ) based on
    * the successors configurations of the CURRENT Configuration, stop searching recursively when finding a terminal case (no more moves possible or ennemy in your spawn)
    * @param n amount of row of the board
    * @param m amount of colmun of the board
    * @param configuration current configuration
    * @param currentPlayer 0 -> Black / 1 -> White
    * @param zobrist The Zobrist Hashing Object that contains all generated keys to calculate hashCode of each configuration
    * @return 
    */
    public static int mainFct(int n, int m, int[][] configuration, int currentPlayer,ZobristHexapawn zobrist) {
        /* Memoization Find Hashcode for the given configuration and save it into configHash */ 
        zobrist.resetHash();
        int configHash = findZobristHash(configuration,n,m,zobrist);
        
        /**
         * Check if the current configuration has already been calculated in mainFct by exploring the HashMap
         * Returns the corresponding value if it's the case
         */
        if(memo.containsKey(configHash)) {
            return memo.get(configHash);
        }
        /* Check if an enemy pawn has already reached your spawn, if it's the case return 0, that means currentPlayer has lost the game*/
        if (isEnemyLastLine(currentPlayer, configuration, n, m)) {
            return 0;
        }
        /* List to save successors hashCode (keys) and successors config (config) */
        ArrayList<Integer> keys = new ArrayList<>();
        ArrayList<int[][]> successors = nextConfigs(currentPlayer, configuration, n, m,keys,zobrist);
        /* If successors is empty after the search of next_configs, if none are found, no more moves are possible, that means currentPlayer has lost the game */
        if (successors.isEmpty()) {
            return 0;
        }
        /* successors configurations values array */
        Integer[] maxValues = new Integer[successors.size()];
        boolean negativeFlag = false;

        if (currentPlayer == 1) {
            currentPlayer = 0;
        } else {
            currentPlayer = 1;
        }

        for (int configIndex = 0; configIndex < successors.size(); configIndex++) {
            if (memo.containsKey(keys.get(configIndex))) {
                maxValues[configIndex] = memo.get(keys.get(configIndex));
            } else {
                /* Recursive search of configurations successors values*/
                maxValues[configIndex] = mainFct(n, m, successors.get(configIndex), currentPlayer,zobrist);
                /*
                * Memoization
                * not sure of the utility of this put in the hashMap
                */
                memo.put(keys.get(configIndex),maxValues[configIndex]);
            }

            /** Check if there is negative values among successors configurations values.
             */
            if (maxValues[configIndex] == 0) {
                return 1;
            }
            if (maxValues[configIndex] < 0) {
                negativeFlag = true;
            }
        }

        if (negativeFlag) {
            /** Use the Mathematical formula of Q1 to find currentConfiguration value and push the result to the hashMap*/
            int value = (-1 * where(maxValues)) + 1;
            memo.put(configHash,value);
            return value;
        }
        /** Use the Mathematical formula of Q1 to find currentConfiguration value and push the result to the hashMap*/
        int greatestPositive = Collections.max(Arrays.asList(maxValues));
        int value = (-1 * greatestPositive) - 1;
        memo.put(configHash,value);
        return value;
    }
    
    
    /**
     * 
     * @param configuration configuration to be calculated
     * @param n amount of row of the game board
     * @param m amout of column of the game board
     * @param zobrist Zobrist Hash values of each possibility
     * @return Hash value of the configuration
     */
    private static int findZobristHash(int[][] configuration,int n,int m,ZobristHexapawn zobrist) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if(configuration[i][j] != -1) {
                    zobrist.add(configuration[i][j], i*j);
                }
            }
        }
        return zobrist.getHash();
    }

    public static void main(String[] args) throws Exception {
        System.out.println("Here is a data input example : \n");
        System.out.println("3\n4\n␣p␣p\np␣␣␣\n␣␣␣P");
        System.out.println();

        Scanner myScan = new Scanner(System.in);
        
        System.out.println("Enter n (vertical): ");
        int n = Integer.parseInt(myScan.nextLine());
        System.out.println("Enter m (horizontal): ");
        int m = Integer.parseInt(myScan.nextLine());
        int[][] inputConfig = new int[n][m];

        
        System.out.println("Enter configuration : ");
        System.out.println();
        // ------------------------------------------------------------LECTURE DE LA DATA
        for (int x = 0; x < n; x++) {
            char[] values = myScan.nextLine().toCharArray();
            for (int y = 0; y < m; y++) {
                if (values[y] == 'P') {
                    inputConfig[x][y] = 1;
                } else if (values[y] == 'p') {
                    inputConfig[x][y] = 0;
                } else {
                    inputConfig[x][y] = -1;
                }
            }
        }

        System.out.println(mainFct(n, m, inputConfig,1,new ZobristHexapawn(n, m)));
        
        
        
        myScan.close();
    }
}

