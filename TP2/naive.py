import numpy as np

n = int(input()) #vertical
m = int(input())
inputConfig = np.zeros((n,m),dtype=int)       # [[ 0, 0, 0, 0],
                                #  [-1,-1,-1,-1],
                                #  [ 1, 1, 1, 1]]
### ----------------------------------------------------- LECTURE DE LA DATA
for x in range(n) :
    values = input()
    for y in range(m) :
        if values[y] == "P" :
            inputConfig[x][y] = 1
        elif values[y] == "p":
            inputConfig[x][y] = 0
        else :
            inputConfig[x][y] = -1
#print(inputConfig)

### ----------------------------------------------------- FONCTIONS UTILES

def isEnemyLastLine (currentPlayer , configuration, n,m) : #current player is either 0 or 1
    if currentPlayer == 1 :
        for i in range(m) :
            if (configuration[n - 1][i] == 0) : # Check presence of black pawn on last row of board
                return True 
    else :
        for i in range(m) :
            if (configuration[0][i] == 1) : # # Check presence of white pawn on first row of board
                return True 
    return False



## Scan the list representating the configuration to find all its succesors configurations
def next_configs(currentPlayer, configuration, n, m) :
    configs = []
    if currentPlayer == 1 :
        for x in range(n) :
            for y in range(m) :
                if configuration[x][y] == 1 :
                    if x - 1 > -1 : ## Checking borders of the board
                        if configuration[x - 1][y] == -1 :         # Going forward (Free space in front of the pawn)
                            configs.append(permute(configuration,(x,y),(x-1,y))) # Appending to all possible configs, the configuration where the current pawn moved forward
                                                   
                        if y - 1 > -1 : ## Checking borders
                            if configuration[x - 1][y - 1] == 0 : ## Testing if we can move by killing the ennemy pawn in diagonal left
                                configs.append(permute(configuration,(x,y),(x-1,y-1)))# Appending to all possible configs, the configuration where the current pawn moved in diagonal left by eating the ennemy pawn
                        if  y + 1 < m : ## Checking borders
                            if configuration[x - 1][y + 1] == 0: ## Testing if we can move by killing the ennemy pawn in diagonal right
                                configs.append(permute(configuration,(x,y),(x-1,y+1))) # Appending to all possible configs, the configuration where the current pawn moved in diagonal right by eating the ennemy pawn
    elif currentPlayer == 0 :
         for x in range(n) :
            for y in range(m) :
                if configuration[x][y] == 0 :
                    if x + 1 < n : ## Checking borders of the board
                        if configuration[x + 1][y] == -1 :
                            configs.append(permute(configuration,(x,y),(x+1,y)))
                        if  y - 1 > -1 : ## Checking borders of the board
                            if configuration[x + 1][y - 1] == 1 : ## Testing if we can move by killing the ennemy white pawn in diagonal left
                                configs.append(permute(configuration,(x,y),(x+1,y - 1)))
                        if y + 1 < m : ## Checking borders of the board
                            if configuration[x + 1][y + 1] == 1: ## Testing if we can move by killing the ennemy white pawn in diagonal right
                                configs.append(permute(configuration,(x,y),(x+1,y+1)))
    return configs

## Returns a configuration in which a pawn moved given its position, destination and original board
def permute(currentConfiguration, position,destination) :
    newConfig = currentConfiguration.copy()
    """
    for x in range(n) :
        for y in range(m) :
            newConfig[x][y] = currentConfiguration[x][y]
    """
    #np.copyto(newConfig,currentConfiguration,casting='same_kind', where=True)
    tmp = currentConfiguration[position[0]][position[1]] 
    newConfig[position[0]][position[1]] = -1 
    newConfig[destination[0]][destination[1]] = tmp
    
    return newConfig

### ----------------------------------------------------- MAIN

def mainFct(n,m,configuration,currentPlayer) :
    if isEnemyLastLine(currentPlayer,configuration,n,m) :
        return 0
    successors = next_configs(currentPlayer,configuration,n,m)
    if len(successors) == 0 : # Si la liste est vide
        return 0
    maxValues = np.zeros(len(successors))
    negativeFlag = False

    if currentPlayer == 1 :
        currentPlayer = 0
    else :
        currentPlayer = 1

    for configuration in range(len(successors)) :
        maxValues[configuration] = mainFct(n,m,successors[configuration],currentPlayer)
        if maxValues[configuration] == 0 :
            return 1
        if maxValues[configuration] < 0 :
            negativeFlag = True
    
    if negativeFlag :
        return ((-1 * (np.where(maxValues < 0, maxValues, np.NINF).max())) + 1)
    return ((-1 * (maxValues.max())) - 1)

#print("Results : \n ---")
print(mainFct(n,m,inputConfig,1))
print("Config successors : \n")
