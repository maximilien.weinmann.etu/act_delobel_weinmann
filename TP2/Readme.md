# TP2 Hexapawn
## Jeremy Delobel 
## Maximilien Weinmann 

### Lien du git 

https://gitlab.univ-lille.fr/maximilien.weinmann.etu/act_delobel_weinmann/-/tree/main/TP2

### Mails 
jeremy.delobel.etu@univ-lille.fr

maximilien.weinmann.etu@univ-lille.fr

---

### Lancer le code :

Il suffit de se rendre dans TP2/algos/src, faire javac *.java, puis java lefichier

## Q1 ) Donnez une formule mathématique permettant de calculer la valeur d’une configuration à partir des valeurs de ses successeurs. Il peut être utile de distinguer le cas où un des successeurs au moins a une valeur négative du cas où ils sont tous positifs.

Dans le cas ou tous les successeurs de la configuration sont positifs on aura :
    -1 * (max(successeurs) - 1)

Sinon si on a des successeurs négatifs et positifs on aura :
    -1 * (max(successeurs négatifs) + 1)

## Q2 ) Codez une version naïve de cette fonction, qui ne mémorise pas ses résultats. Soumettez-la sur la plate-forme de test. Elle sera testée sur des instances « faciles » à résoudre.

Nous avons dans un premier temps implémenté une version naïve de l'algorithme Hexpawn en utilisant le langage **Python**.
Cette première version nous a demandé beaucoup de temps et de réfléxion car nous avions du mal à visualiser l'algorithme dans son ensemble.

Grâce aux conseils donnés par nos camarades et Mr.Derbel en cours nous avons eu une idée plus claire de la structure de l'algorithme à partir de la seconde séance de TP.

Un des premiers jet concernant le squelette de l'algorithme a été le suivant :

```
Formule (Config, Joueur) {
    - Gestion des Cas d'arrêts (Pions ennemis du Joueur arrivés en bout de course/ plus de configurations "filles possible")
    - Calculer les listes des configurations possibles :
        - CaD stocker les tableaux après chaque déplacements AUTORISES des pions du Joueur actuel)
    - Pour chaque configuration (recursivité pour trouver la  valeur de la configuration par rapport a ses configurations successeuses)
        - Gestion du cas ou une configuration successeuse est terminale
    - Application de la formule mathématique de la Q1 et return de cette valeur.
}
```

Ce squelette en tête nous avons commencé par implémenter les sous-fonctions permettant de gêrer les cas d'arrêts. 
Cela passe par la vérification de la présence de pions sur les lignes extrêmes du plateau de jeu ainsi que l'absence de configurations successeuses.

```py
# /!\ Les fonctions ci-dessous ne sont pas toutes fonctionelles, l'idée était plutôt d'illustrer le début de notre processus de réflexion

## Fonction permettant d'identifier si un pion adverse se trouve sur la ligne de départ du joueur courrant.
function isEnemyLastLine (currentPlayer, configuration,n,m) # currentPlayer = 0 -> noir / currenPlayer =  1 -> blanc
    if(currentPlayer == 1) : # Joueur courrant -> BLANC
        for (i = 0;i < m ;i ++) :
            if (configuration[m-1][i] != 1) : # Checking de présence de pions noirs dans la dernière ligne du plateau de jeu
                return True 
    else :
        for (i = 0;i < m ;i ++) : # Joueur courrant -> NOIR
            if (configuration[0][i] != 0) :# Checking de présence de pions blancs dans la première ligne du plateau de jeu
                return True 

    return False

def next_configs(currentPlayer, configuration, n, m) :
    configs = []
    if currentPlayer == 1 :
        for x in range(n) :
            for y in range(m) :
                if configuration[x][y] == 1 :
                    if x - 1 > -1 : ## Checking borders of the board
                        if configuration[x - 1][y] == -1 :         # Going forward (Free space in front of the pawn)
                            #Permut et add
                                                   
                        if y - 1 > -1 : ## Checking borders
                            if configuration[x - 1][y - 1] == 0 : ## Testing if we can move by killing the ennemy pawn in diagonal left
                                #Permut et add
                        if  y + 1 < m : ## Checking borders
                            if configuration[x - 1][y + 1] == 0: ## Testing if we can move by killing the ennemy pawn in diagonal right
                                #Permut et add
    elif currentPlayer == 0 :
         for x in range(n) :
            for y in range(m) :
                if configuration[x][y] == 0 :
                    if x + 1 < n : ## Checking borders of the board
                        if configuration[x + 1][y] == -1 :
                           #Permut et add
                        if  y - 1 > -1 : ## Checking borders of the board
                            if configuration[x + 1][y - 1] == 1 : ## Testing if we can move by killing the ennemy pawn in diagonal left
                                #Permut et add
                        if y + 1 < m : ## Checking borders of the board
                            if configuration[x + 1][y + 1] == 1: ## Testing if we can move by killing the ennemy pawn in diagonal right
                               #Permut et add
    return configs
```

Notre première version de l'algorithme naif réalisé en Python peut-être retrouvée au sein du fichier [naive.py](./naive.py)
Divers fichiers de textes peuvent être trouvé dans le dossier *skribble*, ils contiennent diverses notes et observations que nous avons pu faire au fil des semaines.

Nous avons dans un premier temps réalisé plusieurs tests fonctionnels à partir de diverses configurations pour tester **le bon fonctionnement** de notre algorithme.
Les tests étant concluant nous avons souhaité mettre à l'épreuve notre algorithme via le site [contest](https://contest.fil.univ-lille.fr/contest/classic/show/tp2-master1-hexapawn).

**Malheureusement**, l'utilisation de **la bibliothèque numpy** nous a empêché de tester notre algorithme sur le site, tous les tests passaient au rouge.
![Tests Python sur Contest](./snapshots/test-naivepy.png)

Nous avons donc traduit cet algorithme naif en **Java**, d'une part car cela nous permettait de pouvoir lancer la batterie de tests via le site contest, mais également pour gagner en temps d'éxécution, Java étant plus rapide que Python.

Notre version de l'algorithme naif réalisé en Java peut-être retrouvée au sein du fichier [Naif.java](./algos/src/Naif.java).
La traduction du programme fut assez rapide sauf pour l'implémentation de la *deepcopy* de la configuration mère en fille ainsi que la conception de divers fonctions étant réalisés par *numpy* dans notre version en Python.

Nous avons donc pu tester notre algorithme naif sur le site [contest](https://contest.fil.univ-lille.fr/contest/classic/show/tp2-master1-hexapawn) et obtenir les résultats suivants :
![Tests Java naive sur Contest](./snapshots/test-naivejava.png)


Nous validons 7 des 8 tests présent sur le site, un résultat assez inattendu pour nous, n'ayant pas encore utiliser la programmation dynamique

--- 

## Question 3 : Codez une version « avec mémoïzation » de la fonction d’évaluation. Soumettez-la sur la plateforme de test.

Afin d'implémenter de la mémoïsation dans notre algorithme Hexapawn nous avons implémenté une **HashMap** afin d'associer une clé unique représentant une configuration ainsi que sa valeur.
Pour cela nous avons testé diverses implémentations en faisant varier la clé (type et méthode de calcul).

### Première version DynamiqueV1, une clé sous forme de String ?

Avec cette méthode, le but était d'implémenter le principe de la mémoisation le plus rapidement et simplement possible, en modifiant le moins possible notre algorithme naif.

Pour cela nous avons choisi de prendre pour **clé** la **fonction Array.toString() chaque configuration** et sa **valeur**
Il s'agissait ensuite de placer des vérifications de la présence des **clés** à des endroits dans le code.   

```
/* Pseudo Code de l'algorithme dynamique*/
- Lancer la fonction recursive pour les valeurs données en input
- clé_config présente dans hashMap ? return valeur associé à la clé
- ennemyLastLine ? return 0 (perdu)
- Calcul des configurations successeuses ()
- configuration vide ? return 0 (perdu)
- Pour chaque config_fille dans successors :
    -clé_config_fille dans hashMap ? stocker valeur
    -Appel recursive sur config_fille
    - Trouver la valeur de la configuration courrante via la formule q1
    - Stocker le couple clé_config,valeur dans la HashMap
    - Retourner la valeur
```

Vous pouvez trouver notre implémentation de cette version dynamique utilisant **une chaine de caractères** comme **clé de HashMap** dans le fichier [DynamiqueV1.java](./algos/src/DynamiqueV1.java)

Cette version nous permet d'obtenir les résultats aux tests de **contest** suivants :
![test-dynamiqueV1](./snapshots/test-dynamiqueV1.png)

**Etonnamment cette version dynamique ne valide pas le test 7 précédemment validé par la version naive.**
N'étant pas satisfaits des résultats obtenus nous avons cherché d'autres moyens de générer une clé unique pour chaque configuration tout en évitant au maximum les collisions lors des opérations relatives à la HashMap.


### Une seconde version DynamiqueV2, des clés entières et uniques (faux)?

Nous avons choisi pour cette seconde version d'essayer de créer une clé sous forme d'un entier **Integer** plutôt qu'une chaine de caractères **String**. 

Pour calculer cet identifiant unique nous avons souhaité nous inspirer d'une des méthodes de calcul de l'identifiant d'un marqueur (QRCode) réalisé dans l'un des TPs du cours de **PJE-D**

Voici notre démarche de calcul de l'identifiant expliqué:

![PJE-D Methodo](./snapshots/ExplicationPJED.png)

Vous pouvez retrouver notre implémentation de cette version dans le fichier [DynamiqueV2.java](./algos/src/DynamiqueV2.java)

Malheureusement lors des tests sur *contest* nous avons remarqué un problème auquel nous n'étions pas préparés. 
Ci-dessous l'aperçu des résultats des tests avec **DynamiqueV2**
![test-dynamiqueV2](./snapshots/test-dynamiqueV2.png)

En effet, nous pouvons voir non seulement que plus de la moitié des tests passent au rouge mais en plus, qu'il s'agisse d'erreurs sur le résultat et non pas sur la vitesse comme on pouvait s'y attendre.

Nous nous sommes alors rendu compte que notre méthode de calcul inspirée de *PJE-D* ne fonctionnait pas car ici la base étant de 3 0vide/1noir/2blanc au lieu d'une base binaire 0noir/1blanc.

Cela signifie que plusieurs configurations avaient en réalité le même identifiant ici et donc renvoyait de mauvaises valeurs.

Nous avons donc abandonné l'utilisation de cette méthode de calcul d'identifiant et avons cherché un autre moyen de créer une clé unique à partir d'une configuration.

## Une nouvelle méthode plus efficace ? DynamiqueV3 et le Zobrist Hashing.

Suite à l'échec de *DynamiqueV2* nous avons cherché un nouveau moyen d'obtenir une *clé entière (et unique)* pour chaque configuration.

Nous avons decouvert une méthode de hâchage, très utilisée dans des algorithmes pour des jeux de plateau comme le *jeu de Go* ou les *échecs*.

Il s'agit du **Zobrist Hashing** qui doit son nom a son inventeur l'informaticien **Albert Lindsey Zobrist**.

Cette méthode consiste à **générer des valeurs aléatoires entières** pour chaque **pièce** possibles sur chaque case du tableau.

Dans le cas d'Hexapawn on à **2 pièces possibles** : 
-   Pion Noir
-   Pion Blanc

La case vide n'est pas considérée comme une pièce dans cette méthode.

Dans le cas d'un plateau de 3 lignes * 4 colonnes on aura donc 2 * 3 * 4
valeurs aléatoires à générer.

On peut ensuite trouver le hashCode() d'une configuration en la parcourant et en réalisant une opération XOR entre le hashCode actuel et la valeur aléatoire générée à l'indice ```[piece][position]```
Une fois la configuration parcourue, on a donc son hashCode associé.

La ou cette méthode devient vraiment intéressante, c'est pour le calcul du hashCode des configurations successeurs à celle courrante.
En effet effectuer de simples opérations XOR entre les valeurs des clés des éléments supprimées et ajoutées et le hashCode de la configuration courrante permettent d'obtenir le hashCode des configurations "filles".
On peut alors très simplement et rapidement savoir si une configuration "fille" est présente dans la Map ou non (sans avoir besoin de la parcourir en N^2).

Nous sommes assez fiers de l'utilisation de cette méthode de hachage car elle nous permet de valider 7/8 tests du site Contest, et ce en un temps plus rapide que ce que nous avons pu observer chez nos camarades (notamment pour le test N°7).

Ci-dessous le résultat des tests de Contest avec notre algorithme **DynamiqueV3**.

![test-dynamiqueV3](./snapshots/test-dynamiqueV3.png)

Si on compare aux performances obtenues pour notre algorithme naif on a :


<table>
    <thead>
        <th> Numéro Test </th>
        <th> Temps naif Java </th>
        <th> Temps dynamiqueV3 </th>
    </thead>
    <tbody>
        <tr>
            <td>Test 1</td>
            <td>Succès (0.116s)</td>
            <td>Succès (0.12s)</td>
        </tr>
        <tr>
            <td>Test 2</td>
            <td>Succès (0.12s)</td>
            <td>Succès (0.124s)</td>
        </tr>
        <tr>
            <td>Test 3</td>
            <td>Succès (0.176s)</td>
            <td>Succès (0.14s)</td>
        </tr>
        <tr>
            <td>Test 4</td>
            <td>Succès (0.532s)</td>
            <td>Succès (0.164s)</td>
        </tr>
        <tr>
            <td>Test 5</td>
            <td>Succès (0.188s)</td>
            <td>Succès (0.156s)</td>
        </tr>
        <tr>
            <td>Test 6</td>
            <td>ECHEC TIMED_OUT</td>
            <td>ECHEC Wrong Value</td>
        </tr>
        <tr>
            <td>Test 7</td>
            <td>Succès (4.868s)</td>
            <td>Succès (0.452s)</td>
        </tr>
        <tr>
            <td>Test 8</td>
            <td>Succès (0.148s)</td>
            <td>Succès (0.14s)</td>
        </tr>
    </tbody>
</table>

Nous pensons que l'erreur du test 6 pour la version dynamique V3 vient d'une erreur de collision parmi les valeurs générées.

---

## Pistes d'améliorations.

Nous avons songé à diverses pistes d'améliorations (plus ou moins efficaces) pour améliorer nos différents algorithmes.

- Changer la manière de trouver et de sauvegarder les configurations des configurations successeurs (**nextConfigs(...)**)
- Ne plus convertir l'input en int (-1,0,1) et garder les configurations sous la forme de tableaux de caractères.
- Eviter les collisions dans les HashMap() en ajoutant la donnée sur le joueur courrant dans le hashCode de la configuration.
- Faire 2 HashMap distinctes, 1 pour chaque joueur pour éviter les collisions.

## Mot de la fin

Nous avons pu appliquer durant ce TP le **principe de la mémoization** pour la valeur des configurations.
Nous avons découvert une nouvelle méthode de Hashing avec le **Zobrist Hashing**.
Petite piqure de rappel sur la différence entre *shallow* et *deep* copy, ainsi que la façon dont sont passés les paramètres (value et référence) en Python et Java.

PS : Si vous avez une idée de comment nous pourrions rendre l'algorithme *dynamiqueV3* fiable à 100 %, nous serions très heureux de l'apprendre.


