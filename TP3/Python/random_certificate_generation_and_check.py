import random

def randomGenerator(k, n, listeDePoidsDesObjets) :
    liste_valeur_sac_indice_objet = []
    print(listeDePoidsDesObjets)
    for obj in listeDePoidsDesObjets :
        liste_valeur_sac_indice_objet.append(random.randint(0, k - 1))
    return liste_valeur_sac_indice_objet

def verifier(certificat, k, n, c, listeDePoidsDesObjets) :
    liste_poids_des_sacs = [0 for x in range(k)]
    for index in range(n) :
        liste_poids_des_sacs[certificat[index]] += listeDePoidsDesObjets[index]
    for poids in liste_poids_des_sacs :
        if poids > c :
            return False
    return True