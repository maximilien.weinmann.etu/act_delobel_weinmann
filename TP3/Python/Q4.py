import generate_and_check_all_certificates, random_certificate_generation_and_check
print("Enter a file, like ../Donnees/BinPack/exBPeq1_min3\n")

listeDePoidsDesObjets = []
with open(input()) as data :
    c = int(data.readline())        # c est la capacité d'un sac
    n = int(data.readline())        # n est le nombre d'objets
    for ligne in range(n) :
        listeDePoidsDesObjets.append(int(data.readline()))

print("How many bags do we have ?\n")

k = int(input())

print("\nNumber of bags = ", k, ", number of objects = ", n, ", capacity of a bag = ", c, ", the objects's list of weight : ", listeDePoidsDesObjets, "\n")

print("What mode are we using ? \n1 is vérification, 2 is non-déterministe, 3 is exploration exhaustive\n")

choice = int(input())

if choice == 1 :
    print("You selected vérification mode.\n")
    print("Enter a certificate\n")
    certificate = input().split(",")
    certificate[0] = certificate[0][1:]
    certificate[-1] = certificate[-1][:-1]
    for x in certificate :
        if x[0] == " " :
            x = x[1:]
    certificate = [int(x) for x in certificate]
    if generate_and_check_all_certificates.verifier(certificate, k, n, c, listeDePoidsDesObjets) :
        print("The algorithm says True, because your certificate is valid.\n")
    else :
        print("The algorihtm says False, because your certificate isn't valid.\n")
elif choice == 2 :
    print("You selected non-determinist mode.\n")
    certificate = random_certificate_generation_and_check.randomGenerator(k, n, listeDePoidsDesObjets)
    print("The randomly generated certificate is : ", certificate, "\n")
    if random_certificate_generation_and_check.verifier(certificate, k, n, c, listeDePoidsDesObjets) :
        print("The algorithm says True, this randomly generated certificate is valid.\n")
    else :
        print("The algorihtm says False, this randomly generated certificate isn't valid.\n")
elif choice == 3 :
    print("You selected exploration exhaustive mode.\n")
    print("Make sure k and n aren't to big, as this mode might take a while...\n")
    liste_des_certificats = generate_and_check_all_certificates.generator(k, n)
    if generate_and_check_all_certificates.verificator_all_certificates(liste_des_certificats, k, n, c, listeDePoidsDesObjets) :
        print("The algorithm says True, as you can see on the above line, there is a solution to this problem.\n")
    else :
        print("The algorihtm says False, there is no solution to this problem.\n")