import generate_and_check_all_certificates
print("Enter a file, like ../Donnees/exPart1_True\n")

k = 2
listeDePoidsDesObjets = []
with open(input()) as data :
    n = int(data.readline())        # n est le nombre d'objets
    for ligne in range(n) :
        listeDePoidsDesObjets.append(int(data.readline()))

c = sum(listeDePoidsDesObjets) / 2

print("Make sure k and n aren't to big, as this mode might take a while...\n")
liste_des_certificats = generate_and_check_all_certificates.generator(k, n)
if generate_and_check_all_certificates.verificator_all_certificates(liste_des_certificats, k, n, c, listeDePoidsDesObjets) :
    print("The algorithm says True, as you can see on the above line, there is a solution to this problem.\n")
else :
    print("The algorihtm says False, there is no solution to this problem.\n")