def horlogiste(certificat, k, n) :
    certificat[-1] += 1
    for index in range(n - 1, 0, -1) :
        if certificat[index] == k :
            certificat[index - 1] += 1
            certificat[index] = 0
    return certificat

def generator(k, n) :
    index_certificat = 1
    certificat = [0 for x in range(n)]
    liste_des_certificats = [[0 for x in range(n)]]
    
    nombre_de_certificat = k ** n
    while index_certificat < nombre_de_certificat :
        certificat = horlogiste(certificat, k, n)
        liste_des_certificats.append([x for x in certificat])
        index_certificat += 1
    return liste_des_certificats

def verifier(certificat, k, n, c, listeDePoidsDesObjets) :
    liste_poids_des_sacs = [0 for x in range(k)]
    for index in range(n) :
        liste_poids_des_sacs[certificat[index]] += listeDePoidsDesObjets[index]
    for poids in liste_poids_des_sacs :
        if poids > c :
            return False
    return True

def verificator_all_certificates(liste_des_certificats, k, n, c, listeDePoidsDesObjets) :
    for certificat in liste_des_certificats :
        isValid = verifier(certificat, k, n, c, listeDePoidsDesObjets)
        if isValid :
            print(certificat)
            return True
    return False