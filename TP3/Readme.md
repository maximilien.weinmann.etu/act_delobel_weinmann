### Jeremy Delobel && Maximilien Weinmann



[Lien vers le gitlab](https://gitlab.univ-lille.fr/maximilien.weinmann.etu/act_delobel_weinmann/-/tree/main/TP3)

### À Savoir,

Au début nous avons fait les certificats de telles sorte que :
On avait une liste, la sous liste avait k le nombre de sacs sous liste, et chaque sous liste contenant les objets qui y avaient été placé
On s'est rendu compte que c'est génant alors on a changé de voie.


Pour lancer nos algos, il suffit de se rendre dans le dossier Python de TP3 dans un terminal

Puis de lancer avec 
``````
python3 lefichier.py
``````
et de vous laisser guider par les instructions du terminal
Nous vous conseillons de suivre le Readme en parallèle pour des explications d'aglorithme ou de raisonnement.  

## __1 Qu'est ce qu'une propriété NP ?__

### __Q1 La propriété est NP.__
#### __*Définir une notion de certificat*__

De manière générale, un certificat est une manière de présenter une solution à un problème.

##### __*On rappelle que :*__

* n = le nombre d'objets
* k = le nombre de sacs
* c = la capacité d'un sac

#### __*Comment pensez-vous l'implémenter ?*__

Ici, pour BinPack, nous donnons pour certificat 
un tableau de taille n, pour lequel chaque valeur est l'indice d'un sac, et chaque indice de valeur représente un objet différent.

La taille d'un certificat est n.

#### __*La taille des certificats est-elle bien bornée polynomialement par rapport à la taille de l’entrée ?*__

Binpack a pour entrée : liste de taille n + un nombre de sac

La taille de l'entrée est n + 1.

Notre certificat est de taille n, donc |c| est <= Q(|u|)

La taille de notre certificat est bornée polynomialement par la taille de l'entrée puisque n <= n + 1

#### __*Proposez un algorithme de vérification associé.*__

Français :
On considère une liste avec autant de valeur que de sac, chaque valeur représente le poids du sac, on l'appelle la liste de poids des sacs.  

On a la liste des objets, étant une liste de valeurs pour laquelle chaque valeur est le poids de l'objet.  

Pour chaque mention d'un sac dans le certificat, ajouter à ce sac mentionné (dans la liste de poids des sac) le poids de l'objet, le poids de l'objet est à l'indice de la mention du sac dans la liste des objets

Pour chaque poids de la liste de poids des sacs, si ce poids excède la capacité d'un sac, retourner Faux  

Si aucun poids n'excede la capacité d'un sac, retourner Vrai


Pseudocode :  

``````
liste_poids_des_sacs = [0 for x in range(k)]
pour indice dans range(len(certificat)) :
    liste_poids_des_sacs[certificat[indice]].append(listeDePoidsDesObjets[index])
pour poids in liste_poids_des_sacs :
    if poids > c :
        return False
return True
``````

#### __*Est-il bien polynomial ?*__
La complexité de l'algorithme de vérification est O(n + k).  
Donc l'algorithme est polynomial.  

### __Q2 NP = Non déterministe polynomial__

#### __Q2.1 Génération aléatoire d'un certificat.__

Voir le code random_certificate_generation.py pour le code python  

Pseudocode : 
``````
    def alea_generation(k, listeDesObjets) :
        certificat = []
        for x in range(len(listedesObjets)) :
            certificat.append(randomInteger(0, k - 1))
        return certificat
``````

#### __*Votre algorithme génère-t-il de façon uniforme les certificats, i.e. tous les certificats ont-ils la même probabilité d’apparaître ?*__

Oui chaque certificat à la même chance d'apparaître.

#### __Q2.2 Quel serait le schéma d’un algorithme non-déterministe polynomial pour le problème ?__

À faire

Tirer aléatoirement un certificat, on a une chance d'obtenir une configuration gagnante, ça prend en général un temps exponentiel.

### __Q3 N P ⊂ EXPTIME__

#### __Q3.1 Pour n et k fixés, combien de valeurs peut-prendre un certificat ?__

Un certificat peut prendre k<sup>n</sup> valeurs pour n et k fixés.

#### __Q3.2 Enumération de tous les certificats__

Nous proposons l'ordre où le certificat le plus petit est celui où chaque objet est dans le premier sac, et le plus grand est celui où chaque objet est dans le dernier sac.

Plus un indice du certificat est faible, et plus sa valeur est proche de k, et plus le certificat est grand, et nous faisons de sorte que, si

n = 4  
k = 3 (il existe donc le sac 0, le sac 1 et le sac 2)  

Le plus petit certificat est [0, 0, 0, 0]  
juste ensuite vient [0, 0, 0, 1]  
juste ensuite vient [0, 0, 0, 2]  
juste ensuite vient [0, 0, 1, 0]  
juste ensuite vient [0, 0, 1, 1]  
juste ensuite vient [0, 0, 1, 2]  
juste ensuite vient [0, 0, 2, 0]  
juste ensuite vient [0, 0, 2, 1]  
...  
et le plus grand certificat est [2, 2, 2, 2]

#### __Q3.3 L’algorithme du British Museum__

Il suffit de vérifier chaque cas, un par un, du plus petit au plus grand pour suivre le concept de l’algorithme du British Museum.  

Si en procédant ainsi, on trouve qu'au moins 1 certificat est valide, alors le problème a au moins une solution

*trouver la complexité d'une verification
*refaire un algo de verification  

La complexité de l'algorithme est O(Temps de verification d'un certificat * k<sup>n</sup>)


### __Q4 Implémenter les notions et algorithmes évoqués ci-dessus__ 
C'est fait, dans Q4.py

Pour lancer l'algorithme en réponse à cette question :  

Assurez-vous d'être dans le dossier Python de TP3 dans un terminal

Puis de lancer avec 
``````
python3 Q4.py
``````

## 2__Réductions polynomiales
Une première réduction très simple
### Q1)

Voyons une instance de Partition :

Données : Une liste t de valeur numérique, la liste étant de taille n
Peut on séparer la liste en deux sous listes t1 et t2, de telles sorte que la somme des valeurs<sub>t1i<sub> soit égale à la moitié de la somme de toutes les valeurs de la liste t ?

Le problème est de taille n, la taille de la liste

Exemple :
Nous nous réferons au fichier Données/exPart1_True  

On a alors une liste t de 5 éléments [12, 7, 4, 4, 5]

Nous pouvons partitionner de la sorte :

une liste t1 [12 + 4] et une liste t2 [5, 7, 4]

Le tout vaut 12 + 7 + 4 + 4 + 5 soit 32.

La sous liste t1 vaut 12 + 4 = 16

Le résultat est True
La propriété est vérifiée.  


Grâce à l'exemple on peut facilement modéliser une instance de Partition dans BinPack

Soit J l'instance de BinPack définie par :

- n Le nombre de valeur correspond au nombre d'objet  
- k Les deux sous listes correspondent aux nombre de sacs (k sera toujours = 2)
- x1, x2, ..., xn les valeurs correspondent aux poids des objets
- c n'existe pas vraiment dans Partition, mais on peut dire par convention que c == la somme des valeurs divisé par deux et correspond à la capacité d'un sac, ainsi on s'assure que c n'est jamais dépassé
- True if la somme des valeurs divisée par 2 correspond au plus à la capacité d'un sac, False sinon


La construction est polynomiale car la taille des données d'entrées de Partition est très proche de la taille des données d'entrées de BinPack (à 2 constantes près)
### SSI
Suposons que J est valide :

la somme des valeurs divisée par 2 correspond effectivement à la capacité d'un sac

On trouve une répartition des objets dans les sacs tels que chaque objet se retrouve dans un sac sans éxcéder la capacité des sacs  

I est valide.  

### SSI
Supposons que I une instance de Partition, est valide :

On trouve deux sous liste dont la somme des valeurs vaut la somme de toutes les valeurs divisée par 2
J est valide

### Q1.1)
Voir le fichier Part2Q1_1.py

### Q1.2)
En considérant que la propriété Partition est connue NP-Complète, cela signifie que Partition est NP-Dure et NP.  

On peut donc affirmer que la propriété BinPack est NP-Dure car nous avons réussi à réduire la propriété connue Partitions polynomialement en BinPack.  

Puisque nous savons que BinPack est aussi NP, en plus d'être NP-dure, alors nous déduisons que BinPack est NP-complet.

### Q1.3) Pensez-vous que BinPack se réduise polynomialement dans P artition ? Pourquoi ?
Oui car les deux problemes sont NP-complets.

### Q2)
Partition peut être <strong>presque</strong> vu comme un cas particulier de Sum, le cas
ou c vaut la somme des xn entiers divisé par 2.

Nous en déduisons que Partition peut être réduit en Sum.

### Q3)
Pour la propriété Partition nous sommes contraint d'utiliser la totalité des valeurs pour résoudre le problème (idem pour BinPack),
en effet les deux sous-listes doivent être égale à la moitié de la somme des valeurs.
Cette contrainte n'est pas présente pour la résolution de sum, on peut pour la propriété sum se contenter de piocher x valeurs parmis les n disponibles pour résoudre le problème.

Voyons une instance de Sum :

Données : Une liste t de valeur numérique, la liste étant de taille n ainsi qu'une valeur cible c
Peut on obtenir un sous ensemble de t (t1) avec une taille de 1 à n pour lequel la somme des xi présent dans t soient égal à c.

Le problème est de taille n, la taille de la liste

Exemple :
Nous nous réferons au fichier Données/exSum2_True  

On a alors une liste t de 7 éléments [2, 12, 8, 4, 6, 10, 4] avec c = 36

On peut donc avoir le sous ensemble t1 tel que S(xi,xi+1, ...) dans t1 = c:

[2, 12, 8, 4, 6, 4] = 36

Le résultat est True
La propriété est vérifiée.  


Grâce à l'exemple précédent on peut facilement modéliser une instance de Sum dans Partition

Soit J l'instance de Partition définie par :

- n Le nombre de valeur correspond au nombre également au nombre de valeur ici  
- x1, x2, ..., xn les valeurs correspondent aux valeurs à partitionner
- c n'existe pas aussi explicitement que dans Sum au sein de Partition en revanche il permet d'établir la valeur que doit avoir le sous-ensemble t1 au sein de partition pour valider sum.


### SSI

Si il existe un sous-ensemble t1 de valeurs dans t pour lequel la somme vaut c, alors la somme des nombres ne faisant pas parties de ce sous ensemble t1 est de la somme totale de t - c.

Donc il existe pour la propriété Partition une partition en deux sous-ensemble tel que leur somme est égale à t-c

### SSI

Supposons qu'il existe une partition t de deux sous-ensembles t1 et t2 tel que leur somme soit égal à la somme de toutes les valeurs - c.
L'un des deux sous-ensembles contient donc la valeur équivalente à la somme de toutes les valeurs SUM(t)-2c. 
En enlevant ce nombre du sous ensemble on obtient un sous-ensemble dont la somme est c et dont tous les nombres sont dans la liste des valeurs initiales.


Voir le fichier Part2Q3.py (cela répond aussi à la Q4), meme si cet algo ne marche pas bien...

## Composition de réduction

### Q4)
Par Transition on obtient :


On a : Sum <= Partition <= BinPack

Puisque Sum se réduit polynomialement dans Partition et qu'on sait que Partition se réduit polynomialement dans BinPack.


Alors : Sum <= BinPack
On peut alors en déduire que Sum se réduit polynomialement dans BinPack.

Pour l'implémentation il suffit alors de passer par l'implémentation de Partition.
Voir le fichier Part2Q3.py

### Q5)

Il s'agit du cas où BinPackDiff où à tous les sacs de meme capacité




Voyons une instance de BinPack :

Données : 
Une liste t d'objets ayant pour valeur leur poids
Un nombre de sachets k
Une capacité pour chaque sachet c.

On cherche si il existe une configuration possible de manière à ce que tous les objets soient réparties dans au maximum k sous-ensembles avec la contrainte que la somme de chaque sous ensemble soit inférieur ou égale à c.

Grâce à l'exemple précédent on peut facilement modéliser une instance de BinPack dans BinPackDiff

Soit J l'instance de BinPackDiff définie par :

Une liste t d'objets ayant pour valeur leur poids correspond à la même chose dans BinPackDiff
Un nombre de sachets k correspond également à un nombre de sachet ici.
Une capacité pour chaque sachet c correspond à la capacité qu'aura chaque sachet dans le problème BinPackDiff.

Les deux problèmes de décision BinPack et BinPackDiff étant très semblables, on se rend assez rapidement compte que BinPack est un cas particulier de BinPackDiff ou chaque sachet à la même capacité.


## 3 __Optimisation versus Decision__

### Q1)
Selon notre compréhension des algorithmes et annotations donnés dans l'énoncé on a :

BinPackOpt1 qui nous retourne le nombre minimal de sachets nécessaires pour la mise en sachets de tout les objets. 
En partant de k=1 l'algorithme BinPackOpt1 va déterminer si il est possible d'obtenir une mise en sachet en fonction d'un x croissant. L'algorithme ira dans le pire des cas soit n(nombre d'objets) sachets. En effet avoir plus de n sachets n'a aucun intêret cela signifirait juste que un ou plusieurs objets devraient être découpés pour être mis en sachet (c'est impossible pour nous).

Il procède alors ainsi : 
```py
def BinPackOpt1(n,t,c) :
    for x in range n :
        if BinPack(x,t,c) == True :
        return x
    else :
        print(not possible)
        return 0
```

BinPackOpt2 est légèrement différent car on lui indique une borne de sac maximale à ne pas franchir (c'est ce que nous avons compris).
En partant de k=1 l'algorithme BinPackOpt2 va déterminer si il est possible d'obtenir une mise en sachet correcte en fonction d'un x croissant. L'algorithme ira dans le pire des cas d'une recherche avec 1 sachet jusqu'à k sachets.
On peut réaliser une écriture alternative de BinpackOpt1 de telle manière à pouvoir la réutiliser dans BinPackOpt2

```py
def BinPackOpt2(k,t,c) :
    for x in range k :
        if BinPack(x,t,c) == True :
        return x
    else :
        print(not possible)
        return 0
```

Dans ce probleme nous connaissons 3 cas :

Le cas où k > n : cas non intéressant car il implique des sacs vides

Le cas où k = n :  Ils auront tous les deux la même performance

Le cas où k < n : BinPackOpt2 est meilleur que BinPackOpt1 car il aura moins de cas à traiter que BinPackOpt1

On utilise BinPack dans BinPackOpt1. Si l'on suppose que le problème d'optimisation BPO1 possède un algorithme de compléxité P alors on peut affirmer la même chose pour son problème de décision associé BinPack. On a alors BinPack = P.


### Q2)
```py
def BinPackOpt1(n,t,c) :
    for x in range n :
        if BinPack(x,t,c) == True :
        return x
    else :
        print(not possible)
        return 0
```

Soit BPO1 un problème d'optimisation associé au problème de décision supposé P Binpack.
On écrit alors la relation sur la compléxité de BP01 qui est de O(n * O(BinPack)) ou n est un nombre entier positif. On peut donc affirmer que BP01 est de la classe P car toujours solvable en temps polynomial.

### Q3)

```py
def BinPackOpt2(k,t,c) :
    for x in range k :
        if BinPack(x,t,c) == True :
        return x
    else :
        print(not possible)
        return 0
```

Soit BPO2 un problème d'optimisation associé au problème de décision supposé P Binpack.
On écrit alors la relation sur la compléxité de BP02 qui est de O(n * O(BinPack)) ou n est un nombre entier positif. On peut donc affirmer que BP02 est de la classe P car toujours solvable en temps polynomial.