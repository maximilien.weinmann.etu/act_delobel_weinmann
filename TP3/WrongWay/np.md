## Q1. Qu'est ce qu'une propriété NP ?

### Q1.1 ) Définir la notion de certificat.
Une liste de sacs remplis avec la totalité des n objets surchargés ou non.

c'est un tableau blabla bla de taille n et ceci cela
### Q1.2) Comment pensez-vous l'implémenter ?

### Q1.3) Quelle sera la taille d’un certificat ? La taille des certificats est-elle bien bornée polynomialement par rapport à la taille de l’entrée ?
La taille d'un certificat sera de n + k.
La taille des certificats est bien bornée polynormialement par rapport à la taille de l'entrée car la taille du certificat est constituée de k qui est une donnée d'entrée.

### Q1.4 )

Venant du code verif_certificat.py :
```
def verif_certificat(n, capacite, k, liste_de_sacs) :
    if k < len(liste_de_sacs) :
        return False
    nombre_objets_comptes = 0
    for sac in liste_de_sacs :
        poids_du_sac = 0
        for objet_in_sac in sac :
            poids_du_sac += objet_in_sac
            nombre_objets_comptes += 1
        if poids_du_sac > capacite :
            return False
    if nombre_objets_comptes != n :
        return False
    return True
```
On peut affirmer que notre algorithme de vérification est en O(n*k) donc polynomial

## Q2. NP = Non déterministe polynomial

### Q2.1) Génération aléatoire d’un certificat.

Venant du code rand_gen.py :
```
def randomGenerator(c, n) :
    listeDePoidsDesObjets = []
    maximumDeSacPourSucces = 3 # à changer en fonction du fichier de données
    sacsRemplisDObjets = [0, 0, 0]

    with open("Donnees/BinPack/exBPeq1_min3") as data :
        c = int(data.readline())
        n = int(data.readline())
        for ligne in range(n) :
            listeDePoidsDesObjets.append(int(data.readline()))
        
        while(listeDePoidsDesObjets) :
            objetPris = listeDePoidsDesObjets[random.randint(0, len(listeDePoidsDesObjets) - 1)]
            sacsRemplisDObjets[random.randint(0, maximumDeSacPourSucces - 1)] += objetPris
            listeDePoidsDesObjets.remove(objetPris)
        return sacsRemplisDObjets
```

Il génère de façon uniforme les certificats, ils ont tous la même probabilité  d'apparaître.
### Q2.2 Quel serait le schéma d’un algorithme non-déterministe polynomial pour le problème ?
Mr Bilel Derbel a dit de passer cette question.

Tirer aléatoirement un certificat, on a une chance d'obtenir une configuration gagnante, ça prend en général un temps exponentiel.
## Q3 NP ⊂ EXPTIME
### Q3.1 Pour n et k fixés, combien de valeurs peut-prendre un certificat ?
Un certificat peut prendre k<sup>n</sup>

### Q3.2 Enumération de tous les certificats

