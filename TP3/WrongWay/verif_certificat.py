def verif_certificat(n, capacite, k, liste_de_sacs) :
    if k < len(liste_de_sacs) :
        return False
    nombre_objets_comptes = 0
    for sac in liste_de_sacs :
        poids_du_sac = 0
        for objet_in_sac in sac :
            poids_du_sac += objet_in_sac
            nombre_objets_comptes += 1
        if poids_du_sac > capacite :
            return False
    if nombre_objets_comptes != n :
        return False
    return True