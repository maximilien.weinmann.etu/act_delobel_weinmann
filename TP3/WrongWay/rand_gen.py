import random
c = 0
n = 0

def randomGenerator(c, n) :
    listeDePoidsDesObjets = []
    maximumDeSacPourSucces = 3 # à changer en fonction du fichier de données
    sacsRemplisDObjets = [0, 0, 0]

    with open("Donnees/BinPack/exBPeq1_min3") as data :
        c = int(data.readline())
        n = int(data.readline())
        for ligne in range(n) :
            listeDePoidsDesObjets.append(int(data.readline()))
        
        while(listeDePoidsDesObjets) :
            objetPris = listeDePoidsDesObjets[random.randint(0, len(listeDePoidsDesObjets) - 1)]
            sacsRemplisDObjets[random.randint(0, maximumDeSacPourSucces - 1)] += objetPris
            listeDePoidsDesObjets.remove(objetPris)
        return sacsRemplisDObjets

print(randomGenerator(c, n))